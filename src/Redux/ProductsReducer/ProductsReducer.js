import productsActionsTypes from "./ActionsTypes";

const initialState = {
  originalProducts: [],
  displayProducts: [],
  selectedProducts: [],
  filterOptions: {
    searchQuery: "",
    sort: "",
  },
  modalProductId: null,
  isOpenProductService: false,
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case productsActionsTypes.RESET_TO_INITIAL_STATE:
      return {
        ...initialState,
      };
    case productsActionsTypes.SET_ORIGINAL_PRODUCTS:
      return {
        ...state,
        originalProducts: action.payload, 
      };

    case productsActionsTypes.SET_DISPLAY_PRODUCTS:
      return {
        ...state,
        selectedProducts: [],
        displayProducts: action.payload,
      };

    case productsActionsTypes.SEARCH_QUERY:
      return {
        ...state,
        filterOptions: {
          ...state.filterOptions,
          searchQuery: action.payload,
        },
      };

    case productsActionsTypes.SORT_TYPE_CHANGED:
      return {
        ...state,
        filterOptions: {
          ...state.filterOptions,
          sort: action.payload,
        },
      };

    case productsActionsTypes.PRICE_RANGE_CHANGED:
      return {
        ...state,
        filterOptions: {
          ...state.filterOptions,
          price: action.payload,
        },
      };

    case productsActionsTypes.SELECT_PRODUCT:
      return {
        ...state,
        selectedProducts: action.payload,
      };

    case productsActionsTypes.SET_MODAL_PRODUCT_ID:
      return {
        ...state,
        modalProductId: action.payload,
      };

    case productsActionsTypes.OPEN_OR_CLOSE_PRODUCT_SERVICE_MODAL:
      return {
        ...state,
        isOpenProductService: action.payload,
      };
    default:
      return state;
  }
};

export default productsReducer;
