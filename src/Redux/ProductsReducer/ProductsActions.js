import productsActionsTypes from "./ActionsTypes";

export const resetToInitialState = () => {
  return {
    type: productsActionsTypes.RESET_TO_INITIAL_STATE,
  };
};

export const getOriginalProducts = products => {
  return {
    type: productsActionsTypes.SET_ORIGINAL_PRODUCTS,
    payload: products,
  };
};

export const getDisplayProducts = products => {
  return {
    type: productsActionsTypes.SET_DISPLAY_PRODUCTS,
    payload: products,
  };
};

export const getSearchQuery = query => {
  return {
    type: productsActionsTypes.SEARCH_QUERY,
    payload: query,
  };
};

export const getSortType = sortType => {
  return {
    type: productsActionsTypes.SORT_TYPE_CHANGED,
    payload: sortType,
  };
};

export const getPriceRange = price => {
  return {
    type: productsActionsTypes.PRICE_RANGE_CHANGED,
    payload: price,
  };
};

export const selectProduct = products => {
  return {
    type: productsActionsTypes.SELECT_PRODUCT,
    payload: products,
  };
};

export const setModalProductId = id => {
  return {
    type: productsActionsTypes.SET_MODAL_PRODUCT_ID,
    payload: id,
  };
};
export const openOrCloseProductServiceModal = bollean => {
  return {
    type: productsActionsTypes.OPEN_OR_CLOSE_PRODUCT_SERVICE_MODAL,
    payload: bollean,
  };
};

