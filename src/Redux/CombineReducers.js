import { createStore } from "redux";
import { combineReducers } from "redux";
import productsReducer from "./ProductsReducer/ProductsReducer";
import UIReducer from "./UIReducer/UIReducer";

const store = createStore(
  combineReducers({
    products: productsReducer,
    UI: UIReducer,
  }),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
