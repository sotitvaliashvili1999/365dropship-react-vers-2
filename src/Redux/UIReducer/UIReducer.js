import UIActionsTypes from "./UIActionTypes";

const initialState = {
  isRegisterOpen: false,
  isLoginOpen: false,
  isBurgerMenuOpen: false,
  isLoggedIn: false,
};

const UIReducer = (state = initialState, action) => {
  switch (action.type) {
    case UIActionsTypes.LOG_IN:
      return {
        ...state,
        isLoggedIn: action.payload,
      };
    case UIActionsTypes.OPEN_REGISTER_MODAL:
      return {
        ...state,
        isLoginOpen: false,
        isRegisterOpen: action.payload,
      };
    case UIActionsTypes.OPEN_LOGIN_MODAL:
      return {
        ...state,
        isRegisterOpen: false,
        isLoginOpen: action.payload,
      };
      case UIActionsTypes.OPEN_BURGER_MENU:
      return{
        ...state,
        isBurgerMenuOpen: action.payload,
      };
    default:
      return state;
  }
};
export default UIReducer;
