import UIActionsTypes from "./UIActionTypes";
export const logInSystem = boolean => {
  return {
    type: UIActionsTypes.LOG_IN,
    payload: boolean,
  };
};
export const openRegisterModal = boolean => {
  return {
    type: UIActionsTypes.OPEN_REGISTER_MODAL,
    payload: boolean,
  };
};

export const openLoginModal = boolean => {
  return {
    type: UIActionsTypes.OPEN_LOGIN_MODAL,
    payload: boolean,
  };
};
export const openBurgerMenu = boolean => {
  return {
    type: UIActionsTypes.OPEN_BURGER_MENU,
    payload: boolean,
  };
};
