export const filterProducts = (originalList, filterOptions) => {
  let array = [...originalList];

  if (filterOptions.searchQuery) {
    array = array.filter(product =>
      product.title
        .toLowerCase()
        .includes(filterOptions.searchQuery.toLocaleLowerCase())
    );
  }

  switch (filterOptions.sort) {
    case "ascPrice":
      array = array.sort((a, b) => a.price - b.price);
      break;
    case "descPrice":
      array = array.sort((a, b) => b.price - a.price);
      break;
    case "ascAlphab":
      array = array.sort((a, b) => a.title.localeCompare(b.title));
      break;
    case "descAlphab":
      array = array.sort((a, b) => b.title.localeCompare(a.title));
      break;
    default:
      array = array.sort((a, b) => a.id - b.id);
  }
  return array;
};

// price
// if (filterOptions.price.length === 2) {
//   array = array.filter(
//     product =>
//       product.price >= filterOptions.price[0] &&
//       product.price <= filterOptions.price[1]
//   );
// }

// export const priceSliderRange = productsList => {
//   let array = productsList.map(product => Number(product.price));
//   let min, max;
//   if (array.length > 0) {
//     min = Math.floor(Math.min(...array));
//     max = Math.ceil(Math.max(...array));
//   }
//   return [min, max];
// };
