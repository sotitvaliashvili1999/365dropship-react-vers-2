import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1480,
      xl: 1920,
      xxl: 2000,   //????????????????????????????

    },
    keys: ["xs", "sm", "md", "lg", "xl", "xxl"],
  },
  pallete: {
    secondary: "#61d5df",
  }
});

