import "./App.css";
import Header from "./Components/Header/Header";
import { Route, Redirect, Switch } from "react-router";
import Profile from "./Components/Profile/Profile";
import Catalog from "./Components/Catalog/Catalog";
import Homepage from "./Components/Homepage/Homepage";
import Dashboard from "./Components/Dashboard/Dashboard";
import Inventory from "./Components/Inventory/Inventory";
import Shoplist from "./Components/Shoplist/Shoplist";
import Transactions from "./Components/Transactions/Transactions";
import Orders from "./Components/Orders/Orders";
import Cart from "./Components/Cart/Cart";
function App() {
  return (
    <div className="app">
      <Header />
      <Switch>
        <Route path="/profile">
          <Profile />
        </Route>
        <Route path="/dashboard">
          <Dashboard />
        </Route>
        <Route path="/catalog">
          <Catalog />
        </Route>
        <Route path="/inventory">
          <Inventory />
        </Route>
        <Route path="/cart">
          <Cart />
        </Route>
        <Route path="/orders">
          <Orders />
        </Route>
        <Route path="/transactions">
          <Transactions />
        </Route>
        <Route path="/shoplist">
          <Shoplist />
        </Route>
        <Route path="/homepage">
          <Homepage />
        </Route>
        <Redirect to="/homepage"></Redirect>
      </Switch>
    </div>
  );
}

export default App;
