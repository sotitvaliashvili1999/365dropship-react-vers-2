import Tint from "../Common/Tint";
import { useEffect } from "react";
import { tokenValidity } from "../API/API";
const Cart = () => {
  useEffect(() => {
    tokenValidity();
  }, []);
  return (
    <div className="temporary__wrapper">
      <Tint />
      <h1 className="temporary__header">C A R T</h1>
    </div>
  );
};
export default Cart;
