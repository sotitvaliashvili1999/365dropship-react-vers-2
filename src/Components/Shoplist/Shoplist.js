import Tint from "../Common/Tint";
import { useEffect } from "react";
import { tokenValidity } from "../API/API";
const Shoplist = () => {
  useEffect(() => {
    tokenValidity();
  }, []);
  return (
    <div className="temporary__wrapper">
      <Tint />
      <h1 className="temporary__header">S H O P L I S T</h1>
    </div>
  );
};
export default Shoplist;
