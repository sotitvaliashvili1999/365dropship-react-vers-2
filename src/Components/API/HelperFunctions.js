import {
  removeFromCart,
  addToCart,
  createNewProduct,
  updateProduct,
  deleteProduct,
} from "./API";

export const setLocalStorage = result => {
  localStorage.setItem("user", JSON.stringify(result.data.data));
  localStorage.setItem("token", result.data.data.token);
  localStorage.setItem("isAdmin", result.data.data.isAdmin);
};

export const removeLocalStorage = () => {
  localStorage.removeItem("user");
  localStorage.removeItem("token");
  localStorage.removeItem("isAdmin");
};

export const productService = (service, values, id) => {
  if (service === "productAdd") {
    return createNewProduct(values);
  } else if (service === "productEdit") {
    return updateProduct(id, values);
  } else if (service === "productDelete") {
    return deleteProduct(id);
  }
};

export const addSelectedProductsToCart = async selectedProducts => {
  try {
    for (const id of selectedProducts) await addToCart(id, 1);
  } catch (err) {
    throw err;
  }
};

export const removeSelectedProductsFromCart = async selectedProducts => {
  try {
    for (const id of selectedProducts) await removeFromCart(id);
  } catch (err) {
    throw err;
  }
};
