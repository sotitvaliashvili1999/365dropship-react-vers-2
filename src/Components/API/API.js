import axios from "axios";
import { setLocalStorage, removeLocalStorage } from "./HelperFunctions";
const SERVER_URL = "http://18.185.148.165:3000/";
const SERVER_URL_V1 = "http://18.185.148.165:3000/api/v1/";

axios.interceptors.request.use(config => {
  config.headers.Authorization = `Bearer ${localStorage.getItem(`token`)}`;
  return config;
});

axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error.response.status === 401) {
      removeLocalStorage();
      if (
        window.location.pathname !== "/homepage" &&
        window.location.pathname !== "/homepage/register" &&
        window.location.pathname !== "/homepage/login"
      )
        window.location.href = "/homepage";
    }
    return Promise.reject(error);
  }
);

//Registration
export const registration = async info => {
  try {
    const result = await axios.post(SERVER_URL + "register", info);
    setLocalStorage(result);
    return result.data;
  } catch (err) {
    throw new Error(err);
  }
};

// login
export const login = async info => {
  try {
    const result = await axios.post(SERVER_URL + "login", info);
    setLocalStorage(result);
    return result.data;
  } catch (err) {
    throw new Error(err);
  }
};

// token validity
export const tokenValidity = async () => {
  try {
    const result = await axios.get(SERVER_URL_V1 + `cart`);
    return result.data;
  } catch (err) {
    throw err;
  }
};

// add product to cart
export const addToCart = async (productId, qty) => {
  try {
    const result = await axios.post(SERVER_URL_V1 + "cart/add", {
      productId,
      qty,
    });
    return result.data;
  } catch (err) {
    throw err;
  }
};

// get cart
export const getCart = async () => {
  try {
    const result = await axios.get(SERVER_URL_V1 + `cart`);
    return result.data.data.cartItem.items;
  } catch (err) {
    throw err;
  }
};

// remove product from Inventory
export const removeFromCart = async id => {
  try {
    const result = await axios.post(SERVER_URL_V1 + `cart/remove/${id}`);
    return result.data;
  } catch (err) {
    throw err;
  }
};

// update cart
export const updateCart = async (id, qty) => {
  try {
    const result = await axios.post(SERVER_URL_V1 + `cart/update/${id}`, {
      qty,
    });
    return result.data.data.cartItem.items;
  } catch (err) {
    throw new Error(err);
  }
};

// create new product
export const createNewProduct = async data => {
  try {
    const result = await axios.post(SERVER_URL_V1 + "products", data);
    return result.data;
  } catch (err) {
    throw err;
  }
};

// update existing product
export const updateProduct = async (id, data) => {
  try {
    const result = await axios.put(SERVER_URL_V1 + `products/${id}`, data);
    return result.data;
  } catch (err) {
    throw err;
  }
};

// delete existing product
export const deleteProduct = async id => {
  try {
    const result = await axios.delete(SERVER_URL_V1 + `products/${id}`);
    return result.data;
  } catch (err) {
    throw err;
  }
};

//  Fetch Products
const fetchProducts = async url => {
  try {
    const result = await axios.get(
      SERVER_URL_V1 + (url ? `products/${url}` : "products")
    );
    return result.data.data;
  } catch (err) {
    throw err;
  }
};

// get all products
export const getAllProducts = async () => await fetchProducts();

// get single product
export const getSingleProduct = async id => await fetchProducts(id);

export const updateUserInfo = async (id, data) => {
  try {
    const result = await axios.put(SERVER_URL_V1 + `users/${id}`, data);
    return result;
  } catch (err) {
    throw err;
  }
};
