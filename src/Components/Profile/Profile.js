import "./Profile.css";
import ProfileHeader from "./ProfileHeader";
import EditUserInfo from "./EditUserInfo";
import { NavLink, Switch, Route, Redirect } from "react-router-dom";
import { tokenValidity } from "../API/API";
import { useEffect } from "react";
import OpenedBurgerMenu from "../Common/OpenedBurgerMenu";

const Profile = () => {
  
  useEffect(() => tokenValidity(), []);

  return (
    <div className="profile__wrapper">
      <ProfileHeader />
      <div className="profile__body">
        <div className="profile__card">
          <div className="card__header">
            <div className="headdings__wrapper">
              <NavLink
                to="/profile/userinfo"
                className="card__heading"
                activeClassName="active__heading"
              >
                <div className="card__heading profile">profile</div>
              </NavLink>
              <NavLink
                to="/profile/billing"
                className="card__heading"
                activeClassName="active__heading"
              >
                <div className="card__heading billing">Billing</div>
              </NavLink>
              <NavLink
                to="/profile/invoice"
                className="card__heading"
                activeClassName="active__heading"
              >
                <div className="card__heading invoice">invoice history</div>
              </NavLink>
            </div>
            <div className="card__button deactivate__button">
              deactivate profile
            </div>
          </div>
          <div className="card__body">
            <OpenedBurgerMenu/>
            <Switch>
              <Route path="/profile/userinfo">
                <EditUserInfo />
              </Route>
              <Route path="/profile/billing"></Route>
              <Route path="/profile/invoice"></Route>
              <Redirect to="/profile/userinfo"></Redirect>
            </Switch>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Profile;
