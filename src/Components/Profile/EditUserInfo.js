import "./EditUserInfo.css";
import VisibilityIcon from "@material-ui/icons/Visibility";
import profileIcon from "../../icons/profile.png";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import { Formik, Form, ErrorMessage } from "formik";
import { TextField, InputAdornment } from "@material-ui/core";
import { updateUserInfoSchema } from "../Homepage/Schema";
import { useState, useEffect } from "react";
import { updateUserInfo } from "../API/API";
import { useSnackbar } from "notistack";
import { useHistory } from "react-router-dom";
import { useStyles } from "./useStyles";
import { Button } from "@material-ui/core";

const EditUserInfo = () => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
  });
  const [passwordVisible, setPasswordVisible] = useState(false);

  const { push } = useHistory();

  const fillUpUserState = () => {
    setUser({
      firstName: JSON.parse(localStorage.getItem("user")).firstName,
      lastName: JSON.parse(localStorage.getItem("user")).lastName,
      email: JSON.parse(localStorage.getItem("user")).email,
      password: "",
    });
  };

  useEffect(() => fillUpUserState(), []);

  const handleSubmit = values => {
    const userId = JSON.parse(localStorage.getItem("user")).id;
    delete values.passwordConfirmation;
    updateUserInfo(userId, values)
      .then(result => {
        localStorage.setItem("user", JSON.stringify(result.data.data));
        fillUpUserState();
        enqueueSnackbar("Information has been Updated successfully", {
          variant: "success",
          autoHideDuration: 1500,
        });
        push("/profile");
      })
      .catch(err => {
        enqueueSnackbar("Information hasn't been Updated successfully", {
          variant: "error",
          autoHideDuration: 1500,
        });
        push("/profile");
      });
  };

  return (
    <Formik
      enableReinitialize
      initialValues={{
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        password: user.password,
        passwordConfirmation: "",
      }}
      onSubmit={handleSubmit}
      validationSchema={updateUserInfoSchema}
    >
      {({ values, handleChange, handleBlur, errors, touched }) => {
        return (
          <Form className="personal-info__form">
            <div className="containers-wrapper">
              <div className="user-info__containers">
                <h6 className="user-info__heading">PROFILE PICTURE</h6>
                <div className="user-info__body">
                  <div className="profile-photo__container">
                    <img
                      src={profileIcon}
                      alt={"profilePhoto"}
                      className="profile-photo"
                    />
                  </div>
                  <Button
                    variant="contained"
                    disabled
                    className={classes.uploadBtn}
                  >
                    UPLOAD
                  </Button>
                </div>
              </div>
              <div className="user-info__containers">
                <h6 className="user-info__heading">PERSONAL DETAILS</h6>
                <div className="user-info__body">
                  <div className="input__container">
                    <label className="firstName input-label">First Name</label>
                    <TextField
                      className="personal-info__input"
                      name="firstName"
                      variant="outlined"
                      size="small"
                      error={errors.firstName && touched.firstName}
                      value={values.firstName}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    <ErrorMessage
                      component="span"
                      name="firstName"
                      className="personal-info__error-message"
                    />
                  </div>
                  <div className="input__container">
                    <label className="lastName input-label">Last Name</label>
                    <TextField
                      className="personal-info__input"
                      name="lastName"
                      variant="outlined"
                      size="small"
                      error={errors.lastName && touched.lastName}
                      value={values.lastName}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    <ErrorMessage
                      component="span"
                      name="lastName"
                      className="personal-info__error-message"
                    />
                  </div>
                  <div className="input__container">
                    <label className="country input-label">Country</label>
                    <TextField
                      select
                      className="personal-info__input"
                      value="Georgia"
                      variant="outlined"
                      size="small"
                      disabled
                    >
                      <option value="Georgia">Georgia</option>
                    </TextField>
                  </div>
                </div>
              </div>
              <div className="user-info__containers">
                <h6 className="user-info__heading">CHANGE PASSWORD</h6>
                <div className="user-info__body">
                  <div className="input__container">
                    <label className="current-password input-label">
                      Current password
                    </label>
                    <TextField
                      className="personal-info__input"
                      type="password"
                      variant="outlined"
                      size="small"
                      disabled
                    />
                  </div>
                  <div className="input__container">
                    <label className="new-password input-label">
                      New password
                    </label>
                    <TextField
                      name="password"
                      className="personal-info__input"
                      type={passwordVisible ? "text" : "password"}
                      size="small"
                      variant="outlined"
                      error={errors.password && touched.password}
                      value={values.password}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            {passwordVisible ? (
                              <VisibilityIcon
                                className={classes.passwordVisibleIcon}
                                onClick={e => setPasswordVisible(false)}
                              />
                            ) : (
                              <VisibilityOffIcon
                                className={classes.passwordVisibleIcon}
                                onClick={e => {
                                  e.stopPropagation();
                                  setPasswordVisible(true);
                                }}
                              />
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    <ErrorMessage
                      component="span"
                      name="password"
                      className="personal-info__error-message"
                    />
                  </div>
                  <div className="input__container">
                    <label className="repeat-password input-label">
                      Repeat Password
                    </label>
                    <TextField
                      className="personal-info__input"
                      name="passwordConfirmation"
                      type={passwordVisible ? "text" : "password"}
                      size="small"
                      variant="outlined"
                      error={
                        errors.passwordConfirmation &&
                        touched.passwordConfirmation
                      }
                      value={values.passwordConfirmation}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    <ErrorMessage
                      className="personal-info__error-message"
                      component="span"
                      name="passwordConfirmation"
                    />
                  </div>
                </div>
              </div>
              <div className="user-info__containers">
                <h6 className="user-info__heading">CONTACT INFORMATION</h6>
                <div className="user-info__body" id="myForm1">
                  <div className="input__container">
                    <label className="email input-label">Email</label>
                    <TextField
                      className="personal-info__input"
                      name="email"
                      size="small"
                      variant="outlined"
                      error={errors.email && touched.email}
                      value={values.email}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    <ErrorMessage
                      component="span"
                      name="email"
                      className="personal-info__error-message"
                    />
                  </div>
                  <div className="input__container">
                    <label className="skype input-label">Skype</label>
                    <TextField
                      className="personal-info__input"
                      name="skype"
                      value="skype"
                      variant="outlined"
                      size="small"
                      disabled
                    />
                    <ErrorMessage
                      component="span"
                      name="skype"
                      className="personal-info__error-message"
                    />
                  </div>
                  <div className="input__container">
                    <label className="phone input-label">Phone</label>
                    <TextField
                      className="personal-info__input"
                      value="Phone"
                      variant="outlined"
                      size="small"
                      disabled
                    />
                  </div>
                </div>
              </div>
            </div>
            <Button
              variant="contained"
              className={classes.saveChangesBtn}
              type="submit"
            >
              Save Changes
            </Button>
          </Form>
        );
      }}
    </Formik>
  );
};
export default EditUserInfo;
