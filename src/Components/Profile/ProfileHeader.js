import "./ProfileHeader.css";
import BurgerMenu from "../Common/BurgerMenu";
import HelpButton from "../Common/HelpButton";
import { useHistory } from "react-router";
import { removeLocalStorage } from "../API/HelperFunctions";
import { useDispatch } from "react-redux";
import { openBurgerMenu } from "../../Redux/UIReducer/UIActions";
const ProfileHeader = () => {
  const dispatch = useDispatch();
  
  const { push } = useHistory();
  
  const logOut = () => {
    removeLocalStorage();
    push("/homepage");
  };
  
  const onClickBurgerMenu = () => dispatch(openBurgerMenu(true));

  return (
    <div className="profile__header">
      <h2 className="profile__heading">MY PROFILE</h2>
      <div className="header__rigth-side">
        <div className="header__logout-btn" onClick={logOut}>
          Log out
        </div>
        <HelpButton />
        <BurgerMenu onClick={onClickBurgerMenu}/>
      </div>
    </div>
  );
};
export default ProfileHeader;
