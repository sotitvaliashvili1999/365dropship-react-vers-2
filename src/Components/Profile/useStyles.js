import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles({
  uploadBtn: {
    backgroundColor: "rgb(97,213,223)",
    fontFamily: "Gilroy-medium",
  },
  saveChangesBtn: {
    backgroundColor: "rgb(97,213,223)",
    color: "#fff",
    fontFamily: "Gilroy-semibold",
    alignSelf: "flex-end",
    marginRight: "65px",
    marginBottom: "20px",
  },
  passwordVisibleIcon:{
    cursor: "pointer",
  }
});
