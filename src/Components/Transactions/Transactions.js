import Tint from "../Common/Tint";
import { useEffect } from "react";
import { tokenValidity } from "../API/API";

const Transactions = () => {
  useEffect(() => {
    tokenValidity();
  }, []);

  return (
    <div className="temporary__wrapper">
      <Tint />
      <h1 className="temporary__header">T R A N S A C T I O N S </h1>
    </div>
  );
};
export default Transactions;
