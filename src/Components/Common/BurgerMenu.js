import "./BurgerMenu.css";
import { useMediaQuery } from "@material-ui/core";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { openBurgerMenu } from "../../Redux/UIReducer/UIActions";
const BurgerMenu = ({ onClick }) => {
  
  const mediaQuery = useMediaQuery("(min-width: 1085px)");
  const dispatch = useDispatch();

  useEffect(() => {
    mediaQuery && dispatch(openBurgerMenu(false));
  }, [mediaQuery, dispatch]);

  return (
    <div className="burger__menu" onClick={onClick}>
      <i className="fas fa-bars fa-2x"></i>
    </div>
  );
};
export default BurgerMenu;
