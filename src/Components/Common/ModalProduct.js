import "./ModalProduct.css";
import CatalogButton from "../Catalog/CatalogButton";
import LoadingGif from "./LoadingGif";
import ModalLoading from "../../icons/modal Loading.gif";
import { getSingleProduct } from "../API/API";
import { useEffect, useState } from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import { setModalProductId } from "../../Redux/ProductsReducer/ProductsActions";
import { useDispatch } from "react-redux";

const ModalProduct = ({ productId, buttonName, buttonAction, qty }) => {
  const [product, setProduct] = useState({});
  const [productInfoActive, setProductInfoActive] = useState(true);
  const [notFound, setNotFound] = useState(false);
  const dispatch = useDispatch();

  const { push } = useHistory();
  const { path } = useRouteMatch();

  useEffect(() => {
    getSingleProduct(productId)
      .then(product => setProduct(product))
      .catch(err => setNotFound(true));
  }, [productId]);

  const closeModal = () => {
    setNotFound(false);
    setProduct({});
    dispatch(setModalProductId(null));
    push(path === "/catalog/:productService?/:id?" ? "/catalog" : "/inventory");
  };

  const onClickButton = () => buttonAction(product.id, qty);

  const changeSectionOfItemDetail = () =>
    setProductInfoActive(productInfoActive ? false : true);

  return (
    <>
      <div className="product-modal__background" onClick={closeModal}>
        <div className="product__modal" onClick={e => e.stopPropagation()}>
          <div className="exit__btn-container" onClick={closeModal}>
            <i className="exit-btn far fa-times-circle fa-2x"></i>
          </div>
          {notFound && <p className="modal__not-found">Product Not Found</p>}
          {!product.price && !notFound && <LoadingGif gif={ModalLoading} />}
          {product.price && (
            <>
              <div className="modal__left-side">
                <div className="prices__container">
                  <div className="prices__cell RRP__container">
                    <span className="RRP__amount cell__amount">54$</span>
                    <span className="RRP cell__header">RRP</span>
                  </div>
                  <div className="prices__cell cost__container">
                    <span className="cost__amount cell__amount">
                      {product.price} $
                    </span>
                    <span className="cost cell__header">cost</span>
                  </div>
                  <div className="prices__cell profit__container">
                    <span className="profit__amount cell__amount">
                      20% (12%)
                    </span>
                    <span className="profit cell__header">profit</span>
                  </div>
                </div>
                <div className="product-img__container">
                  <img
                    src={product.imageUrl}
                    alt="product"
                    className="modal-product__img"
                  />
                </div>
                <div className="product-img__container-small">
                  <img
                    src={product.imageUrl}
                    alt="product"
                    className="product__img--small"
                  />
                </div>
              </div>
              <div className="modal__right-side">
                <div className="supply-indicators__wrapper">
                  <span className="supply__indicators supply__code">
                    SKU# bgb-s2412499 COPY
                  </span>
                  <span className="supply__indicators supplier">
                    Supplier: <span>SP-Supplier115</span>
                  </span>
                </div>
                <div className="product-title__wrapper">
                  <p className="product-modal__title">{product.title}</p>
                </div>
                <div className="product-add-btn__wrapper">
                  <div
                    className="modal__counter-div"
                    onClick={e => e.stopPropagation()}
                  ></div>
                  <CatalogButton
                    name={buttonName}
                    className="product-modal__button"
                    onClick={onClickButton}
                  />
                </div>
                <div className="product-info__wrapper">
                  <div
                    className={
                      "product-info__header-wrapper " +
                      (productInfoActive ? "product-info__wrapper-active" : "")
                    }
                    onClick={changeSectionOfItemDetail}
                  >
                    <p className="product__info">Product details</p>
                  </div>
                  <div
                    className={
                      "product-info__header-wrapper " +
                      (!productInfoActive ? "product-info__wrapper-active" : "")
                    }
                    onClick={changeSectionOfItemDetail}
                  >
                    <p className="shipping__rates product__info">
                      Shipping Rates
                    </p>
                  </div>
                </div>
                <div className="product__description-wrapper">
                  {productInfoActive && (
                    <p className="product__description">
                      {product.description}
                    </p>
                  )}
                  {!productInfoActive && (
                    <p className="product__description"></p>
                  )}
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default ModalProduct;
