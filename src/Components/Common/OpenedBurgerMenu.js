import "./OpenedBurgerMenu.css";
import dropShipIcon from "../../icons/365Icon.png";
import ProfileIcon from "../Header/ProfileIcon";
import CloseIcon from "@material-ui/icons/Close";
import { Drawer, List, ListItemIcon, ListItemText } from "@material-ui/core";
import NavItem from "../Header/NavItem";
import { NavLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { openBurgerMenu } from "../../Redux/UIReducer/UIActions";
import { useStyle } from "./useStyles";

const OpenedBurgerMenu = () => {

  const classes = useStyle();

  const burgerMenuOpen = useSelector(state => state.UI.isBurgerMenuOpen);
  const dispatch = useDispatch();

  const closeBurgerMenu = () => dispatch(openBurgerMenu(false));


  return (
    <Drawer open={burgerMenuOpen} onClick={closeBurgerMenu} anchor="right">
      <div className="burger-menu__wrapper" onClick={e => e.stopPropagation()}>
        <div className="burger-menu__header">
          <NavLink to="/homepage" onClick={closeBurgerMenu}>
            <img
              src={dropShipIcon}
              alt="dropShipIcon"
              className="drop-ship__icon"
            />
          </NavLink>
          <CloseIcon
            onClick={closeBurgerMenu}
            className={classes.burgerMenuCloseIcon}
          />
        </div>
        <List component="nav" className={classes.list}>
          <NavLink
            to="/profile"
            className="nav-link"
            activeClassName="nav-link__active"
            onClick={closeBurgerMenu}
          >
            <ListItemText
              primary="Profile"
              className={classes.listItemText}
            ></ListItemText>
            <ListItemIcon>
              <ProfileIcon />
            </ListItemIcon>
          </NavLink>
          <NavLink
            to="/dashboard"
            className="nav-link"
            activeClassName="nav-link__active"
            onClick={closeBurgerMenu}
          >
            <ListItemText
              primary="Dashboard"
              className={classes.listItemText}
            ></ListItemText>
            <ListItemIcon>
              <NavItem name="dashboard" />
            </ListItemIcon>
          </NavLink>
          <NavLink
            to="/catalog"
            className="nav-link"
            activeClassName="nav-link__active"
            onClick={closeBurgerMenu}
          >
            <ListItemText
              primary="Catalog"
              className={classes.listItemText}
            ></ListItemText>
            <ListItemIcon>
              <NavItem name="catalog" />
            </ListItemIcon>
          </NavLink>
          <NavLink
            to="/inventory"
            className="nav-link"
            activeClassName="nav-link__active"
            onClick={closeBurgerMenu}
          >
            <ListItemText
              primary="Inventory"
              className={classes.listItemText}
            ></ListItemText>
            <ListItemIcon>
              <NavItem name="inventory" />
            </ListItemIcon>
          </NavLink>
          <NavLink
            to="/cart"
            className="nav-link"
            activeClassName="nav-link__active"
            onClick={closeBurgerMenu}
          >
            <ListItemText
              primary="Cart"
              className={classes.listItemText}
            ></ListItemText>
            <ListItemIcon>
              <NavItem name="cart" />
            </ListItemIcon>
          </NavLink>
          <NavLink
            to="/order"
            className="nav-link"
            activeClassName="nav-link__active"
            onClick={closeBurgerMenu}
          >
            <ListItemText
              primary="Order"
              className={classes.listItemText}
            ></ListItemText>
            <ListItemIcon>
              <NavItem name="orders" />
            </ListItemIcon>
          </NavLink>
          <NavLink
            to="/transactions"
            className="nav-link"
            activeClassName="nav-link__active"
            onClick={closeBurgerMenu}
          >
            <ListItemText
              primary="Transactions"
              className={classes.listItemText}
            ></ListItemText>
            <ListItemIcon>
              <NavItem name="transactions" />
            </ListItemIcon>
          </NavLink>
          <NavLink
            to="/shoplist"
            className="nav-link"
            activeClassName="nav-link__active"
            onClick={closeBurgerMenu}
          >
            <ListItemText
              primary="Shoplist"
              className={classes.listItemText}
            ></ListItemText>
            <ListItemIcon>
              <NavItem name="shoplist" />
            </ListItemIcon>
          </NavLink>
        </List>
      </div>
    </Drawer>
  );
};
export default OpenedBurgerMenu;
