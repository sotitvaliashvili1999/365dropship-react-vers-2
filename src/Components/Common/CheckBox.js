import "./CheckBox.css";
const CheckBox = ({onChange, checked, className}) => {
  return (
    <div className={"checkbox__wrapper display-"+ className}>
      <label className="container">
        <input
          type="checkbox"
          checked={checked}
          onChange={onChange}
        />
        <span className="checkmark"></span>
      </label>
    </div>
  );
};
export default CheckBox;
