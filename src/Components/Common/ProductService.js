import "./ProductService.css";
import CloseIcon from "@material-ui/icons/Close";
import { Formik, Form } from "formik";
import { useStyle } from "./useStyles";
import { Button } from "@material-ui/core";
import { getSingleProduct } from "../API/API";
import { makeProductSchema } from "../Homepage/Schema";
import { useHistory, useRouteMatch } from "react-router-dom";
import { useState, useEffect } from "react";
import { productService } from "../API/HelperFunctions";
import { useSnackbar } from "notistack";
import { Dialog, TextField } from "@material-ui/core";
import { openOrCloseProductServiceModal } from "../../Redux/ProductsReducer/ProductsActions";
import { useDispatch, useSelector } from "react-redux";

const ProductService = ({ service, id }) => {
  const classes = useStyle();
  const { enqueueSnackbar } = useSnackbar();

  const [product, setProduct] = useState({
    title: "",
    description: "",
    price: "",
    imageUrl: "",
  });
  const [serviceHeader, setServiceHeader] = useState("");
  const isOpenProductService = useSelector(
    state => state.products.isOpenProductService
  );
  const dispatch = useDispatch();

  const history = useHistory();
  const { path } = useRouteMatch();

  useEffect(() => {
    if (service === "productAdd") setServiceHeader("Add new Product");
    else if (service === "productEdit") setServiceHeader("Edit Product");
    else if (service === "productDelete") setServiceHeader("Delete Product");
  }, [setServiceHeader, service]);

  const handleSubmit = values => {
    productService(service, values, id).then(() => {
      if (service === "productAdd")
        enqueueSnackbar("product  has been added successfully", {
          variant: "success",
          autoHideDuration: 1500,
        });
      if (service === "productEdit")
        enqueueSnackbar("product  has been edited successfully", {
          variant: "success",
          autoHideDuration: 1500,
        });
      if (service === "productDelete")
        enqueueSnackbar("product  has been deleted successfully", {
          variant: "success",
          autoHideDuration: 1500,
        });
      setTimeout(() => {
        dispatch(openOrCloseProductServiceModal(false));
        history.push(
          path === "/catalog/:productService?/:id?" ? "/catalog" : "/inventory"
        );
      }, 1600);
    });
  };

  useEffect(() => {
    if (id) {
      getSingleProduct(id)
        .then(data => {
          setProduct(data);
        })
        .catch(() => {
          enqueueSnackbar("invalid Id of product", {
            variant: "error",
            autoHideDuration: 1500,
          });
          setTimeout(
            () =>
              history.push(
                path === "/catalog/:productService?/:id?"
                  ? "/catalog"
                  : "/inventory"
              ),
            1600
          );
          setProduct({
            title: "",
            description: "",
            price: "",
            imageUrl: "",
          });
        });
    }
  }, [id, enqueueSnackbar, history, path]);

  const closeModal = () => {
    dispatch(openOrCloseProductServiceModal(false));
    history.push(
      path === "/catalog/:productService?/:id?" ? "/catalog" : "/inventory"
    );
  };

  return (
    <Dialog open={isOpenProductService} onClick={closeModal}>
      <div
        className="product-service__wrapper"
        onClick={e => e.stopPropagation()}
      >
        <div className="product-service__header">
          <h2 className="product-service__type">{serviceHeader}</h2>
          <CloseIcon className={classes.closeIcon} onClick={closeModal} />
        </div>

        {((service === "productAdd" && !id) ||
          (service === "productEdit" && id) ||
          (service === "productDelete" && id)) && (
          <Formik
            enableReinitialize
            initialValues={
              id
                ? {
                    title: product.title,
                    description: product.description,
                    price: product.price,
                    imageUrl: product.imageUrl,
                  }
                : {
                    title: "",
                    description: "",
                    price: "",
                    imageUrl: "",
                  }
            }
            onSubmit={handleSubmit}
            validationSchema={makeProductSchema}
          >
            {({ touched, values, handleChange, errors, handleBlur }) => {
              return (
                <Form className="form">
                  <div className="field-wrapper">
                    <TextField
                      type="text"
                      name="title"
                      className={classes.textField}
                      error={errors.title && touched.title}
                      label={
                        errors.title && touched.title ? errors.title : "Title"
                      }
                      value={values.title}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      disabled={service === "productDelete"}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </div>
                  <div className="field-wrapper">
                    <TextField
                      type="text"
                      name="description"
                      label={
                        errors.description && touched.description
                          ? errors.description
                          : "Description"
                      }
                      className={classes.textField}
                      error={errors.description && touched.description}
                      value={values.description}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      disabled={service === "productDelete"}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </div>
                  <div className="field-wrapper">
                    <TextField
                      type="text"
                      name="price"
                      className={classes.textField}
                      error={errors.price && touched.price}
                      label={
                        errors.price && touched.price ? errors.price : "price"
                      }
                      value={values.price}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      disabled={service === "productDelete"}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </div>
                  <div className="field-wrapper">
                    <TextField
                      type="text"
                      name="imageUrl"
                      className={classes.textField}
                      error={errors.imageUrl && touched.imageUrl}
                      label={
                        errors.imageUrl && touched.imageUrl
                          ? errors.imageUrl
                          : "image URL"
                      }
                      value={values.imageUrl}
                      onBlur={handleBlur}
                      onChange={handleChange}
                      disabled={service === "productDelete"}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </div>
                  <div className="field-wrapper">
                    {service === "productAdd" && (
                      <Button
                        variant="contained"
                        type="submit"
                        className={classes.submitButton}
                      >
                        Create Product
                      </Button>
                    )}
                    {service === "productEdit" && (
                      <Button
                        variant="contained"
                        type="submit"
                        className={classes.editButton}
                      >
                        save changes
                      </Button>
                    )}
                    {service === "productDelete" && (
                      <Button
                        variant="contained"
                        type="submit"
                        className={classes.deleteButton}
                      >
                        Delete Product
                      </Button>
                    )}
                  </div>
                </Form>
              );
            }}
          </Formik>
        )}
      </div>
    </Dialog>
  );
};

export default ProductService;
