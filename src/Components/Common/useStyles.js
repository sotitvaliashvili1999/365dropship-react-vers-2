import { makeStyles } from "@material-ui/core";

export const useStyle = makeStyles({
  submitButton: {
    backgroundColor: "#61d5df",
    color: "#fff",
    width: "90%",
    marginTop: "10px",
    marginBottom: "50px",
    borderRadius: "4px",
  },
  editButton: {
    backgroundColor: "#57a832",
    width: "90%",
    color: "#fff",
    marginTop: "10px",
    marginBottom: "50px",
    borderRadius: "4px",
  },
  deleteButton: {
    backgroundColor: "#f71414",
    width: "90%",
    marginTop: "10px",
    marginBottom: "50px",
    borderRadius: "4px",
  },
  closeIcon: {
    width: "30px",
    cursor: "pointer",
    position: "absolute",
    right: "20px",
  },
  textField: {
    width: "90%",
    height: "10px",
    fontFamily: "Gilroy-medium",
    marginBottom: "60px",
  },
  settingIcon: {
    position: "absolute",
    right: "20px",
    top: "50px",
    cursor: "pointer",
    color: "rgb(97, 213, 223)",
  },
  editIcon: {
    cursor: "pointer",
    color: "rgb(97, 213, 223)",
  },
  deleteIcon: {
    cursor: "pointer",
    color: "red",
  },
  list: {
    width: "100%",
  },
  listItemText: {
    paddingLeft: "5px",
  },
  burgerMenuCloseIcon: {
    cursor: "pointer",
  },
});
