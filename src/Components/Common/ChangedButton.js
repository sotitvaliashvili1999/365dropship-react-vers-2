import "./ChangedButton.css";
const ChangedButton = ({ onClick, className }) => {
  return (
    <div className={"changed__button "+ className}>
       <i className={className}/>
    </div>
  );
};
export default ChangedButton;
