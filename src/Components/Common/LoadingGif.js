import "./LoadingGif.css";
const LoadingGif = ({gif}) => {
  return (
    <div className="loading-gif__container">
      <img src={gif} alt="lodingGif.png" className="loading__gif"/>
    </div>
  );
};
export default LoadingGif;
