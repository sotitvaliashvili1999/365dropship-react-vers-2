import "./Product.css";
import "../Catalog/CatalogButton.css";
import CheckBox from "./CheckBox";
import SettingsIcon from "@material-ui/icons/Settings";
import EditIcon from "@material-ui/icons/Edit";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router";
import { useState, useEffect } from "react";
import { selectProduct } from "../../Redux/ProductsReducer/ProductsActions";
import { updateCart } from "../API/API";
import { useStyle } from "./useStyles";

const Product = ({ id, img, title, price, buttonName, buttonAction, qty }) => {
  const classes = useStyle();

  const [productSelected, setProductSelected] = useState(false);
  const [singleProductQuantity, setSingleProductQuantity] = useState(qty);
  const [isAdmin, setIsAdmin] = useState(false);
  const [isSettingsOpen, setIsSettingsOpen] = useState(false);
  const selectedProducts = useSelector(
    state => state.products.selectedProducts
  );
  const dispatch = useDispatch();

  const { push } = useHistory();
  const { pathname } = useLocation();

  useEffect(() => {
    let isAdmin = localStorage.getItem("isAdmin");
    if (isAdmin === "true") setIsAdmin(true);
    else setIsAdmin(false);
  }, []);

  useEffect(() => setSingleProductQuantity(qty), [qty]);

  const openModalProduct = () => push(`${pathname}/${id}`);

  const pushToEditProduct = () => {
    push(`${pathname}/productEdit/${id}`);
    setIsSettingsOpen(false);
  };

  const pushToDeleteProduct = () => {
    push(`${pathname}/productDelete/${id}`);
    setIsSettingsOpen(false);
  };

  const onClickProductButton = () => buttonAction(id, singleProductQuantity);

  const decrement = () =>
    setSingleProductQuantity(
      singleProductQuantity - 1 !== 0 ? singleProductQuantity - 1 : 1
    );

  const increment = () => setSingleProductQuantity(singleProductQuantity + 1);

  useEffect(() => {
    let timeout = setTimeout(() => {
      if (buttonName === "Remove Product") {
        updateCart(id, singleProductQuantity);
      }
    }, [500]);
    return () => clearTimeout(timeout);
  }, [singleProductQuantity, buttonName, id]);

  const onClickSettingIcon = () => setIsSettingsOpen(!isSettingsOpen);

  const isProductSelected = e => {
    let array = [...selectedProducts];
    array = array.find(prodId => id === prodId)
      ? array.filter(prodId => id !== prodId)
      : [...array, id];
    dispatch(selectProduct(array));
  };

  useEffect(() => {
    setProductSelected(
      selectedProducts.find(productId => productId === id) ? true : false
    );
  }, [selectedProducts, id]);

  return (
    <div className="product-checkbox-btn__wrapper">
      <CheckBox
        onChange={isProductSelected}
        checked={productSelected}
        className={productSelected ? "block" : ""}
      />
      {isAdmin && (
        <div className="setting-icon__wrapper">
          <SettingsIcon
            className={classes.settingIcon}
            onClick={onClickSettingIcon}
          />
          {isSettingsOpen && (
            <div className="setting-options__wrapper">
              <EditIcon
                className={classes.editIcon}
                onClick={pushToEditProduct}
              />
              <DeleteForeverIcon
                className={classes.deleteIcon}
                onClick={pushToDeleteProduct}
              />
            </div>
          )}
        </div>
      )}
      <div className="btn-wrapper" onClick={onClickProductButton}>
        <button className="catalog__button product__add-button">
          {buttonName}
        </button>
      </div>
      <div
        className={
          "product__wrapper " +
          (productSelected ? "product__wrapper--highlited" : "")
        }
        onClick={openModalProduct}
      >
        <div className="product__img-wrapper">
          <img src={img} alt="Product.png" className="product__img"></img>
        </div>
        <div className="product__title-wrapper">
          <p className="product__title">{title}</p>
        </div>
        <div className="product__supplier-wrapper">
          <p className="product__supplier">
            By: <span>GE supplier K9</span>
          </p>
          <div
            className="product__counter-div"
            onClick={e => e.stopPropagation()}
          >
            <div className="quantity__header">
              <span>Quantity:</span>
            </div>
            <div className="decrement" onClick={decrement}>
              <span>-</span>
            </div>
            <div className="quantity__number">
              <span>{singleProductQuantity}</span>
            </div>
            <div className="increment" onClick={increment}>
              <span>+</span>
            </div>
          </div>
        </div>
        <div className="product__prices-wrapper">
          <div className="rrp__wrapper">
            <span className="product__rrp">$25</span>
            <span className="rrp__header">RRP</span>
          </div>
          <div className="price__wrapper">
            <span className="product__price">${price}</span>
            <span className="price__header">COST</span>
          </div>
          <div className="profit__wrapper">
            <span className="product__profit">15%($5)</span>
            <span className="profit__header">PROFIT</span>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Product;
