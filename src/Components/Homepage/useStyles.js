import { makeStyles } from "@material-ui/styles";
export const useStyles = makeStyles({
  white: {
    marginRight: "5px",
    color: "#ffff",
    fontFamily: "Gilroy-medium",
    fontWeight: 900,
    "&:Hover": {
      color: "#61d5df",
    },
  },
  outlined: {
    marginRight: "5px",
    color: "#61d5df",
    width: "120px",
    fontSize: "16px",
    border: "2px solid #61d5df",
    "&:Hover": {
      backgroundColor: "#61d5df",
      color: "#fff",
    },
  },
  login: {
    marginRight: "5px",
    color: "#61d5df",
    fontFamily: "Gilroy-medium",
    fontWeight: 900,
  },
  logout: {
    marginLeft: "5px",
    marginRight: "5px",
    color: "#fff",
    fontFamily: "Gilroy-medium",
    fontWeight: 900,
    backgroundColor: "#61d5df",
  },
  signUp: {
    width: "170px",
    height: "50px",
    color: "#fff",
    backgroundColor: "#61d5df",
    marginTop: "-50px",
    alignSelf: "center",
    "&:Hover": {
      backgroundColor: "#61d5df",
    },
  },
  closeIcon: {
    width: "30px",
    cursor: "pointer",
  },
  textField: {
    width: "80%",
    height: "20px",
    marginBottom: "50px",
  },
  submitButton: {
    marginTop: "10px",
    marginBottom: "20px",
    width: "80%",
    height: "40px",
    backgroundColor: "#61d5df",
    color: "#fff",
    fontFamily: "Gilroy-medium",
    "&:Hover": {
      backgroundColor: "#61d5df",
    },
  },
  loginTextField: {
    width: "80%",
    height: "30px",
    marginBottom: "30px",
  },
  facebookIcon: {
    color: "blue",
    cursor: "pointer",
  },
  pinterestIcon: {
    color: "rgb(183,8,27)",
    cursor: "pointer",
  },
  passwordVisibleIcon: {
    cursor: "pointer",
  },
});
