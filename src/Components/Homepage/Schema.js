import * as yup from "yup";

export const registrationSchema = yup.object().shape({
  firstName: yup.string().required("First name is required"),
  lastName: yup.string().required("Last name is required"),
  email: yup.string().email("Incorrect Email").required("Email is required"),
  password: yup
    .string()
    .required("Password is required")
    .min(6, "Minimum 6 characters required"),
  passwordConfirmation: yup
    .string()
    .oneOf([yup.ref("password")], "Passwords don't match ")
    .required("Password confirmation is required"),
});

export const loginSchema = yup.object().shape({
  email: yup.string().email("Incorrect Email").required("Email is required"),
  password: yup
    .string()
    .required("Password is required")
    .min(6, "Minimum 6 characters required"),
});

export const makeProductSchema = yup.object().shape({
  title: yup.string().required(),
  description: yup.string().required(),
  price: yup.number().min(0.1).required(),
  imageUrl: yup.string().url().required(),
});

export const updateUserInfoSchema = yup.object().shape({
  firstName: yup.string().required("first Name is required"),
  lastName: yup.string().required("last Name is required"),
  email: yup.string().email().required(),
  password: yup
    .string()
    .required("Password is required")
    .min(6, "Minimum 6 characters required"),
  passwordConfirmation: yup
    .string()
    .oneOf([yup.ref("password")], "Passwords don't match ")
    .required("Password confirmation is required"),
});
