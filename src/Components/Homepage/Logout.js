import { removeLocalStorage } from "../API/HelperFunctions";
import { logInSystem } from "../../Redux/UIReducer/UIActions";
import { useDispatch } from "react-redux";
import { useStyles } from "./useStyles";
import { Button } from "@material-ui/core";


const Logout = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const logOut = () => {
    removeLocalStorage();
    dispatch(logInSystem(false));
  };

  return (
    <Button className={classes.logout} onClick={logOut}>
      Logout
    </Button>
  );
};

export default Logout;
