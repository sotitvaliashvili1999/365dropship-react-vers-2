import "./Login.css";
import EmailIcon from "@material-ui/icons/Email";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import CloseIcon from "@material-ui/icons/Close";
import FacebookIcon from "@material-ui/icons/Facebook";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import PinterestIcon from "@material-ui/icons/Pinterest";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import { Dialog, TextField, Button, InputAdornment } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { Formik, Form } from "formik";
import { loginSchema } from "./Schema";
import { useHistory } from "react-router-dom";
import { useStyles } from "./useStyles";
import { useState } from "react";
import { login } from "../API/API";
import {
  openRegisterModal,
  logInSystem,
  openLoginModal,
} from "../../Redux/UIReducer/UIActions";

const Login = () => {
  const classes = useStyles();

  const [serverError, setServerError] = useState(false);
  const [passwordVisible, setPasswordVisible] = useState(false);
  const isLoginOpen = useSelector(state => state.UI.isLoginOpen);
  const dispatch = useDispatch();

  const { push } = useHistory();

  const openRegister = () => {
    dispatch(openRegisterModal(true));
    push("/homepage/register");
  };

  const closeLogin = () => {
    dispatch(openLoginModal(false));
    push("/homepage");
  };

  const handleSubmit = values => {
    login(values)
      .then(data => {
        if (data.success) {
          dispatch(logInSystem(true));
          dispatch(openLoginModal(false));
          push("/profile");
        }
      })
      .catch(err => {
        dispatch(logInSystem(false));
        setServerError(true);
      });
  };


  return (
    <Dialog open={isLoginOpen} onClick={closeLogin}>
      <div className="login__container" onClick={e => e.stopPropagation()}>
        <div className="login__header">
          <img
            src="https://app.365dropship.com/assets/images/auth/logo.svg"
            alt="icon"
          />
          <h2 className="login__heading">Members Log In</h2>
          <CloseIcon className={classes.closeIcon} onClick={closeLogin} />
        </div>
        <Formik
          enableReinitialize
          initialValues={{ email: "", password: "" }}
          onSubmit={handleSubmit}
          validationSchema={loginSchema}
        >
          {({ values, handleChange, handleBlur, touched, errors }) => {
            return (
              <Form className="login__inputs">
                <TextField
                  type="email"
                  name="email"
                  error={errors.email && touched.email}
                  label={errors.email && touched.email ? errors.email : "Email"}
                  value={values.email}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  variant="outlined"
                  className={classes.loginTextField}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <EmailIcon />
                      </InputAdornment>
                    ),
                  }}
                />
                <TextField
                  type={passwordVisible ? "text" : "password"}
                  name="password"
                  error={errors.password && touched.password}
                  label={
                    errors.password && touched.password
                      ? errors.password
                      : "Password"
                  }
                  onBlur={handleBlur}
                  value={values.password}
                  onChange={handleChange}
                  variant="outlined"
                  className={classes.loginTextField}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <VpnKeyIcon />
                      </InputAdornment>
                    ),
                    endAdornment: (
                      <InputAdornment position="end">
                        {passwordVisible ? (
                          <VisibilityIcon
                            className={classes.passwordVisibleIcon}
                            onClick={e => setPasswordVisible(false)}
                          />
                        ) : (
                          <VisibilityOffIcon
                            className={classes.passwordVisibleIcon}
                            onClick={e => {
                              e.stopPropagation();
                              setPasswordVisible(true);
                            }}
                          />
                        )}
                      </InputAdornment>
                    ),
                  }}
                />
                {serverError && (
                  <div className="login__error-message">
                    <ErrorOutlineIcon />
                    Incorrect Email or Password
                  </div>
                )}
                <Button className={classes.submitButton} type="submit">
                  Log in
                </Button>

                <div className="login-with__header">
                  <div>Log in with</div>
                </div>
                <div className="login-with__wrapper">
                  <FacebookIcon className={classes.facebookIcon} />
                  <PinterestIcon className={classes.pinterestIcon} />
                </div>
                <span className="login__sign-up">
                  Don't have an account?
                  <b onClick={openRegister}> Sign Up</b>
                </span>
              </Form>
            );
          }}
        </Formik>
      </div>
    </Dialog>
  );
};
export default Login;
