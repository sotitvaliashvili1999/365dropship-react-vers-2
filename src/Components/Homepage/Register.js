import "./Register.css";
import CloseIcon from "@material-ui/icons/Close";
import PersonIcon from "@material-ui/icons/Person";
import VpnKeyIcon from "@material-ui/icons/VpnKey";
import EmailIcon from "@material-ui/icons/Email";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import { Dialog, TextField, Button, InputAdornment } from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import { registrationSchema } from "./Schema";
import { removeLocalStorage } from "../API/HelperFunctions";
import { registration } from "../API/API";
import { Formik, Form } from "formik";
import { logInSystem } from "../../Redux/UIReducer/UIActions";
import { useHistory } from "react-router-dom";
import { useStyles } from "./useStyles";
import { useState } from "react";

const Register = () => {
  const classes = useStyles();

  const { push } = useHistory();

  const [serverError, setServerError] = useState(false);
  const [passwordVisible, setPasswordVisible] = useState(false);
  const isRegisterOpen = useSelector(state => state.UI.isRegisterOpen);
  const isLoggedIn = useSelector(state => state.UI.isLoggedIn);
  const dispatch = useDispatch();

  const openLogin = () => push("/homepage/login");

  const closeRegister = () => push("/homepage");

  const logOut = () => {
    removeLocalStorage();
    dispatch(logInSystem(false));
  };

  const handleSubmit = values => {
    registration(values)
      .then(data => {
        if (data.success) push("/profile");
        dispatch(logInSystem(true));
      })
      .catch(err => {
        setServerError(true);
        dispatch(logInSystem(false));
      });
  };
  return (
    <Dialog open={isRegisterOpen} onClick={closeRegister}>
      <div className="form__container" onClick={e => e.stopPropagation()}>
        <div className="form__header">
          <img
            src="https://app.365dropship.com/assets/images/auth/logo.svg"
            alt="icon"
          />
          <h2 className="form__heading">Sign Up</h2>
          <CloseIcon className={classes.closeIcon} onClick={closeRegister} />
        </div>

        {isLoggedIn ? (
          <div className="loggedin-message">
            <p>You are already logged in</p>
            <p>If you want register, please log out first.</p>
            <p className="logout" onClick={logOut}>
              Log out
            </p>
          </div>
        ) : (
          <Formik
            enableReinitialize
            initialValues={{
              firstName: "",
              lastName: "",
              email: "",
              password: "",
              passwordConfirmation: "",
            }}
            onSubmit={handleSubmit}
            validationSchema={registrationSchema}
          >
            {({ values, handleChange, handleBlur, touched, errors }) => {
              return (
                <Form className="form__inputs">
                  <TextField
                    type="text"
                    name="firstName"
                    error={errors.firstName && touched.firstName}
                    label={
                      errors.firstName && touched.firstName
                        ? errors.firstName
                        : "First Name"
                    }
                    variant="outlined"
                    onBlur={handleBlur}
                    value={values.firstName}
                    onChange={handleChange}
                    className={classes.textField}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <PersonIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                  <TextField
                    type="text"
                    name="lastName"
                    error={errors.lastName && touched.lastName}
                    label={
                      errors.lastName && touched.lastName
                        ? errors.lastName
                        : "Last Name"
                    }
                    variant="outlined"
                    onBlur={handleBlur}
                    value={values.lastName}
                    onChange={handleChange}
                    className={classes.textField}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <PersonIcon />
                        </InputAdornment>
                      ),
                    }}
                  />

                  <TextField
                    type="email"
                    name="email"
                    error={errors.email && touched.email}
                    label={
                      errors.email && touched.email ? errors.email : "Email"
                    }
                    variant="outlined"
                    onBlur={handleBlur}
                    value={values.email}
                    onChange={handleChange}
                    className={classes.textField}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <EmailIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                  <TextField
                    type={passwordVisible ? "text" : "password"}
                    name="password"
                    error={errors.password && touched.password}
                    label={
                      errors.password && touched.password
                        ? errors.password
                        : "Password"
                    }
                    variant="outlined"
                    onBlur={handleBlur}
                    value={values.password}
                    onChange={handleChange}
                    className={classes.textField}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <VpnKeyIcon />
                        </InputAdornment>
                      ),
                      endAdornment: (
                        <InputAdornment position="end">
                          {passwordVisible ? (
                            <VisibilityIcon
                              className={classes.passwordVisibleIcon}
                              onClick={e => setPasswordVisible(false)}
                            />
                          ) : (
                            <VisibilityOffIcon
                              className={classes.passwordVisibleIcon}
                              onClick={e => {
                                e.stopPropagation();
                                setPasswordVisible(true);
                              }}
                            />
                          )}
                        </InputAdornment>
                      ),
                    }}
                  />
                  <TextField
                    type={passwordVisible ? "text" : "password"}
                    name="passwordConfirmation"
                    error={
                      errors.passwordConfirmation &&
                      touched.passwordConfirmation
                    }
                    label={
                      errors.passwordConfirmation &&
                      touched.passwordConfirmation
                        ? errors.passwordConfirmation
                        : "Password Confirmation"
                    }
                    variant="outlined"
                    value={values.passwordConfirmation}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    className={classes.textField}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <VpnKeyIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                  {serverError && (
                    <div className="register__error-message">
                      <ErrorOutlineIcon />
                      Account already exists
                    </div>
                  )}
                  <Button className={classes.submitButton} type="submit">
                    Sign Up
                  </Button>

                  <pre className="form__sign-in">
                    Already have an account?
                    <b onClick={openLogin}> Sign in</b>
                  </pre>
                </Form>
              );
            }}
          </Formik>
        )}
      </div>
    </Dialog>
  );
};
export default Register;
