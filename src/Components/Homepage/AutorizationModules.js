import { useParams } from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import { useEffect } from "react";
import {
  openRegisterModal,
  openLoginModal,
} from "../../Redux/UIReducer/UIActions";
import { useDispatch } from "react-redux";

const Autorization = () => {
  
  const { id } = useParams();

  const dispatch = useDispatch();

  useEffect(() => {
    if (id === "register") dispatch(openRegisterModal(true));
    else if (id === "login") dispatch(openLoginModal(true));
  }, [id, dispatch]);
  return (
    <>
      <Register />
      <Login />
    </>
  );
};
export default Autorization;
