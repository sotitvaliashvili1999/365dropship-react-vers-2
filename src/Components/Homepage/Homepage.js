import "./Homepage.css";
import icon from "../../icons/homepageIcon1.png";
import AutorizationModules from "./AutorizationModules";
import Logout from "./Logout";
import { Route, useRouteMatch, useHistory } from "react-router";
import { useSelector, useDispatch } from "react-redux";
import { tokenValidity } from "../API/API";
import { logInSystem } from "../../Redux/UIReducer/UIActions";
import { useEffect } from "react";
import { useStyles } from "./useStyles";
import { NavLink } from "react-router-dom";

import { Button } from "@material-ui/core";

const Homepage = () => {
  const classes = useStyles();

  const isLoggedIn = useSelector(state => state.UI.isLoggedIn);
  const dispatch = useDispatch();

  const { push } = useHistory();
  const { path } = useRouteMatch();

  useEffect(() => {
    tokenValidity()
      .then(() => dispatch(logInSystem(true)))
      .catch(err => {
        if (err.response.status === 401) dispatch(logInSystem(false));
      });
  }, [dispatch]);

  const openRegister = () => {
    push(`${path}/register`);
  };

  const openLogin = () => {
    push(`${path}/login`);
  };

  return (
    <div className="homepage">
      <div className="homepage__header">
        <div className="icon__wrapper">
          <img src={icon} alt="icon" />
        </div>
        <div className="buttons__wrapper">
          <Button className={classes.white}>ABOUT</Button>
          <NavLink to="/catalog" className="homepage__nav-link">
            <Button className={classes.white}>CATALOG</Button>
          </NavLink>
          <Button className={classes.white}>PRICING</Button>
          <Button className={classes.white}>SUPPLIERS</Button>
          <Button className={classes.white}>HELP CENTER</Button>
          <Button className={classes.white}>BLOG</Button>
          <Button
            variant="outlined"
            className={classes.outlined}
            onClick={openRegister}
          >
            REGISTER
          </Button>
          {isLoggedIn ? (
            <Logout />
          ) : (
            <Button className={classes.login} onClick={openLogin}>
              LOGIN
            </Button>
          )}
        </div>
      </div>
      <div className="center__container">
        <img
          src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/356Logo.svg"
          alt="icon"
        />
        <div className="homepage__header">we got your supply chain covered</div>
        <div className="homepage__header">365 days a year!</div>
      </div>
      <Button className={classes.signUp} onClick={openRegister}>
        SIGN UP NOW
      </Button>

      <Route path={`/homepage/:id`}>
        <AutorizationModules />
      </Route>
    </div>
  );
};
export default Homepage;
