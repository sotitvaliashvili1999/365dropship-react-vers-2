import "./Dashboard.css";
import Tint from "../Common/Tint";
import { useEffect } from "react";
import { tokenValidity } from "../API/API";

const Dashboard = () => {

    useEffect(() => {
    tokenValidity();
  }, []);

  return (
    <div className="temporary__wrapper">
      <Tint />
      <h1 className="temporary__header">D A S H B O A R D</h1>
    </div>
  );
};
export default Dashboard;
