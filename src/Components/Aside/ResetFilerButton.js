import "../CatalogButton.css";
import "./ResetFilterButton.css";
const ResetFilterButton = () => {
  return (
    <div className="catalog__button-div reset-filter__button-div">
      <button className="catalog__button reset-filter__button">Reset Filter</button>
    </div>
  );
};
export default ResetFilterButton;
