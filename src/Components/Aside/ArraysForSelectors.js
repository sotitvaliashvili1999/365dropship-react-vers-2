import Australia from "../../countryFlags/Australia.png";
import China from "../../countryFlags/China.png";
import Czech from "../../countryFlags/Czech.png";
import France from "../../countryFlags/France.png";
import German from "../../countryFlags/German.png";
import India from "../../countryFlags/India.png";
import Israel from "../../countryFlags/Israel.png";
import Italy from "../../countryFlags/Italy.png";
import Latvia from "../../countryFlags/Latvia.png";
import Netherland from "../../countryFlags/Netherland.png";
import Poland from "../../countryFlags/Poland.png";
import Singapure from "../../countryFlags/Singapure.png";
import Spain from "../../countryFlags/Spain.png";
import Sweden from "../../countryFlags/Sweden.png";
import Ukraine from "../../countryFlags/Ukraine.png";
import UnitedKingdom from "../../countryFlags/UnitedKingdom.png";
import UnitedStates from "../../countryFlags/USA.png";
import Vanuatu from "../../countryFlags/Vanuatu.png";

export const shipFromList = [
  { flag: Australia, name: "Australia", id: 1 },
  { flag: China, name: "China", id: 2 },
  { flag: Czech, name: "Czech", id: 3 },
  { flag: France, name: "France", id: 4 },
  { flag: German, name: "German", id: 5 },
  { flag: India, name: "India", id: 6 },
  { flag: Israel, name: "Israel", id: 7 },
  { flag: Italy, name: "Italy", id: 8 },
  { flag: Latvia, name: "Latvia", id: 9 },
  { flag: Netherland, name: "Netherlands", id: 10 },
  { flag: Poland, name: "Poland", id: 11 },
  { flag: Singapure, name: "Singapure", id: 12 },
  { flag: Spain, name: "Spain", id: 13 },
  { flag: Sweden, name: "Sweden", id: 14 },
  { flag: Ukraine, name: "Ukraine", id: 15 },
  { flag: UnitedKingdom, name: "United Kingdom", id: 16 },
  { flag: UnitedStates, name: "United States", id: 17 },
  { flag: Vanuatu, name: "Vanuatu", id: 18 },
];

export const supplier = [
  { name: "UK-Supplier108", id: 1 },
  { name: "SP-Supplier118", id: 2 },
  { name: "CN-Supplier127", id: 3 },
  { name: "US-Supplier148", id: 4 },
  { name: "LT-Supplier154", id: 5 },
  { name: "CN-Supplier156", id: 6 },
  { name: "US-Supplier157", id: 7 },
  { name: "CN-Supplier164", id: 8 },
  { name: "NL-Supplier187", id: 9 },
  { name: "US-Supplier176", id: 10 },
  { name: "GE-Supplier177", id: 11 },
];
