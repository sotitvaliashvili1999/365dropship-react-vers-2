import "./Slider.css";
import Slider from "@material-ui/core/Slider";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    width: "95%",
  },
});

const SliderRange = ({ title, symbol, minMax, value, onSetValue }) => {
 
  const classes = useStyles();

  const handleChange = (event, newValue) => {
    onSetValue(newValue);
  };

  return (
    <div className="slider__wrapper">
      <p className="slider__heading">{title} Range</p>
      <Slider
        className={classes.root}
        min={minMax[0]}
        max={minMax[1]}
        value={value}
        onChange={handleChange}
      />
      <div className="slider__indicators-wrapper">
        <div className="amount-from__wrapper">
          <div className="slider__symbol">
            <span className="symbol">{symbol}</span>
          </div>
          <div className="slider__amount">
            <span className="amount">{value[0]}</span>
          </div>
        </div>
        <div className="amount-to__wrapper">
          <div className="slider__symbol">
            <span className="symbol">{symbol}</span>
          </div>
          <div className="slider__amount">
            <span className="amount">{value[1]}</span>
          </div>
        </div>
      </div>      
    </div>
  );
};
export default SliderRange;
