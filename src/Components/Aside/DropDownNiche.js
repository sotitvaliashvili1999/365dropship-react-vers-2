import "./DropDownNiche.css";

const DropDownNiche = ({ onCloseNiche }) => {
  const categories = [
    "Electronics",
    "Jewelery",
    "Men clothing",
    "Women clothing",
  ];

  const onClickCategory = () => {
    onCloseNiche({
      niche: false,
      category: false,
    });
  };

  return (
    <div className="choose-niche__wrapper">
      <ul className="choose-niche__list">
        <li className="choose-niche__option" onClick={onClickCategory}>
          <span>...</span>
        </li>
        {categories.map((category, index) => (
          <li
            className="choose-niche__option"
            id={category}
            onClick={onClickCategory}
          >
            <span>{category}</span>
          </li>
        ))}
      </ul>
    </div>
  );
};
export default DropDownNiche;
