import "./SelectorDropDown.css";
import { useEffect, useState } from "react";
const SelectorDropDown = ({ id, flag, name, onSelectedList, selectedList }) => {
  const [checked, setChecked] = useState(false);

  const onItemClick = e => {
    e.stopPropagation();
    setChecked(checked ? false : true);
    onSelectedList(id);
  };

  useEffect(() => {
    setChecked(selectedList.find(Id => Id === id) ? true : false);
  }, [selectedList, id]);

  return (
    <div className="list__item" key={id} onClick={onItemClick}>
      <input
        type="checkbox"
        className="item__checkbox"
        checked={checked}
        onChange={onItemClick}
      />
      {flag && <img src={flag} alt={name + "png"} className="country__flag" />}
      <span
        className={"item__name " + (checked ? "item__name--highlited" : "")}
      >
        {name}
      </span>
    </div>
  );
};
export default SelectorDropDown;
