import "./ProductSelector.css";
const ProductSelector= ({title, className, onClick,  onChangeArrow}) => {


  return (
    <div className={"product-selector__wrapper product-selector__wrapper--"+ className}  onClick={onClick}>
      <p className={"product-selector__options product-selector__options--"+ className} >
         {title} 
      </p>      
      {!onChangeArrow &&
       <div className="icon">
          <i className="arrow-icon fas fa-sm fa-chevron-down"></i>
       </div> 
      }   
      {onChangeArrow && 
        <div className="icon">
          <i className="arrow-icon fas fa-sm fa-chevron-up"></i>
        </div> 
      }   
         
    </div>
  );
};
export default ProductSelector;
