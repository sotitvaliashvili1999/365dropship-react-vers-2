import "./Aside.css";
import ProductSelector from "./ProductSelector";
import ShippingSelector from "./ShippingSelector";
import CatalogButton from "../Catalog/CatalogButton";
import DropDownNiche from "./DropDownNiche";
import DropDownCategory from "./DropDownCategory";
import SliderRange from "./Slider";
import { useState } from "react";

const Aside = () => {
  const [dropDown, setDropDown] = useState({});
  const [shipOptions, setShipOptions] = useState({});
  const [selectedShipFromList, setSelectedShipFromList] = useState([]);
  const [selectedShipToList, setSelectedShipToList] = useState([]);
  const [selectedSupplierList, setSelectedSupplierList] = useState([]);
  const [profitSliderValue, setProfitSliderValue] = useState([0, 100]);
  const [priceSliderValue, setPriceSliderValue] = useState([0, 20000]);

  const onClickNiche = () => {
    setDropDown({
      niche: !dropDown.niche,
      category: false,
    });
    setShipOptions({
      ...false,
    });
  };

  const onClickCategory = () => {
    setDropDown({
      niche: false,
      category: !dropDown.category,
    });
    setShipOptions({
      ...false,
    });
  };

  const onClickShipFrom = () => {
    setShipOptions({
      shipFrom: !shipOptions.shipFrom,
      shipTo: false,
      supplier: false,
    });
    setDropDown({ ...false });
  };

  const onClickShipTo = () => {
    setShipOptions({
      shipFrom: false,
      shipTo: !shipOptions.shipTo,
      supplier: false,
    });
    setDropDown({ ...false });
  };

  const onClickSupplier = () => {
    setShipOptions({
      shipFrom: false,
      shipTo: false,
      supplier: !shipOptions.supplier,
    });
    setDropDown({ ...false });
  };

  const makeSelectedShipFromList = id => {
    setSelectedShipFromList(prevList =>
      prevList.find(Id => Id === id)
        ? prevList.filter(Id => Id !== id)
        : [...prevList, id]
    );
  };

  const makeSelectedSupplierList = id => {
    setSelectedSupplierList(prevList =>
      prevList.find(Id => Id === id)
        ? prevList.filter(Id => Id !== id)
        : [...prevList, id]
    );
  };

  const makeSelectedShipToList = id => {
    setSelectedShipToList(prevList =>
      prevList.find(Id => Id === id)
        ? prevList.filter(Id => Id !== id)
        : [...prevList, id]
    );
  };

  const onClickResetBtn = () => {
    setSelectedShipFromList([]);
    setSelectedShipToList([]);
    setSelectedSupplierList([]);
    setPriceSliderValue([0, 20000]);
    setProfitSliderValue([0, 100]);
  };

  return (
    <aside className="aside catalog__filter">
      <ProductSelector
        title="Choose Niche"
        classNsame="Niche"
        onChangeArrow={dropDown.niche}
        onClick={onClickNiche}
      />
      {dropDown.niche && <DropDownNiche onCloseNiche={setDropDown} />}
      <ProductSelector
        title="Choose Category"
        className={"Category"}
        onChangeArrow={dropDown.category}
        onClick={onClickCategory}
      />
      {dropDown.category && <DropDownCategory onCloseCategory={setDropDown} />}
      <ShippingSelector
        title="Ship From"
        dropDown={shipOptions.shipFrom}
        onClick={onClickShipFrom}
        onSelectedList={makeSelectedShipFromList}
        selectedList={selectedShipFromList}
        selectedQuantity={selectedShipFromList.length}
      />
      <ShippingSelector
        title="Ship To"
        dropDown={shipOptions.shipTo}
        onClick={onClickShipTo}
        onSelectedList={makeSelectedShipToList}
        selectedList={selectedShipToList}
        selectedQuantity={selectedShipToList.length}
      />
      <ShippingSelector
        title="Select Supplier"
        dropDown={shipOptions.supplier}
        onClick={onClickSupplier}
        onSelectedList={makeSelectedSupplierList}
        selectedList={selectedSupplierList}
        selectedQuantity={selectedSupplierList.length}
      />
      <SliderRange
        title="Price"
        symbol="$"
        minMax={[0, 20000]}
        value={priceSliderValue}
        onSetValue={setPriceSliderValue}
      />
      <SliderRange
        title="Profit"
        symbol="%"
        minMax={[0, 100]}
        value={profitSliderValue}
        onSetValue={setProfitSliderValue}
      />
      <CatalogButton
        name="reset filter"
        divName="filter-reset__button-div"
        className="filter-reset__button"
        onClick={onClickResetBtn}
      />
    </aside>
  );
};
export default Aside;
