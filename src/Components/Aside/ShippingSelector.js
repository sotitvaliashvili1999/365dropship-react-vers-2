import "./ShippingSelector.css";
import { shipFromList, supplier } from "./ArraysForSelectors";
import SelectorDropDown from "./SelectorDropDown";
const ShippingSelector = ({
  title,
  dropDown,
  onClick,
  onSelectedList,
  selectedList,
  selectedQuantity,
}) => {
  return (
    <div className="selector__wrapper">
      <div className="selector__options" onClick={onClick}>
        <p className="selector__header">{title}</p>
        <div className="arrow-quantity__wrapper">
          {selectedQuantity > 0 && !dropDown && (
            <div className="selected__quantity-div">
              <span className="selected__quantity">{selectedQuantity}</span>
            </div>
          )}
          {!dropDown && (
            <div className="icon">
              <i className="arrow__icon--shipping fas fa-xs fa-chevron-down"></i>
            </div>
          )}
          {dropDown && (
            <div className="icon">
              <i className="arrow__icon--shipping fas fa-xs fa-chevron-up"></i>
            </div>
          )}
        </div>
      </div>
      {dropDown && (
        <div className="drop-down__list">
          <div className="list-items__wrapper">
            {title === "Ship From" &&
              shipFromList.map(country => (
                <SelectorDropDown
                  id={country.id}
                  key={country.id}
                  flag={country.flag}
                  name={country.name}
                  onSelectedList={onSelectedList}
                  selectedList={selectedList}
                />
              ))}
            {title === "Ship To" &&
              shipFromList.map(country => (
                <SelectorDropDown
                  id={country.id}
                  key={country.id}
                  flag={country.flag}
                  name={country.name}
                  onSelectedList={onSelectedList}
                  selectedList={selectedList}
                />
              ))}
            {title === "Select Supplier" &&
              supplier.map(supplier => (
                <SelectorDropDown
                  id={supplier.id}
                  key={supplier.id}
                  name={supplier.name}
                  onSelectedList={onSelectedList}
                  selectedList={selectedList}
                />
              ))}
          </div>
        </div>
      )}
    </div>
  );
};
export default ShippingSelector;
