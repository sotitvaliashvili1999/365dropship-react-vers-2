import "./DropDownCategory.css";
const DropDownCategory = ({ onCloseCategory }) => {
  const onClickOption = () => {
    onCloseCategory(false);
  };
  return (
    <div className="choose-category__wrapper">
      <ul className="choose-category__list">
        <li className="choose-category__option" id="" onClick={onClickOption}>
          <span>...</span>
        </li>
      </ul>
    </div>
  );
};
export default DropDownCategory;
