import "./InventoryHeader.css";
import ChangedFilterButton from "../Catalog/Main/CatalogHeader/ChangedFilterButton";
import ChangedSelectButton from "../Catalog/Main/CatalogHeader/ChangedSelectButton";
import ProductQuantity from "../Catalog/Main/CatalogHeader/ProductQuantity";
import BurgerMenu from "../Common/BurgerMenu";
import HelpButton from "../Common/HelpButton";
import InventorySearchInput from "./InventorySearchInput";
import Sort from "../Catalog/Main/CatalogHeader/Sort";
import CatalogButton from "../Catalog/CatalogButton";
import ChangedButton from "../Common/ChangedButton";
import { openBurgerMenu } from "../../Redux/UIReducer/UIActions";
import { useDispatch, useSelector } from "react-redux";
import { selectProduct } from "../../Redux/ProductsReducer/ProductsActions";

const InventoryHeader = ({ onClickRemoveButton }) => {
  const displayProducts = useSelector(state => state.products.displayProducts);
  const selectedProducts = useSelector(state => state.products.selectedProducts);
  const dispatch = useDispatch();

  const onClearSelectedProducts = () => dispatch(selectProduct([]));

  const onSelectAllProducts = () =>
    dispatch(selectProduct(displayProducts.map(prod => prod.id)));

  const onSelectOrClear = () =>
    dispatch(
      selectedProducts.length === displayProducts.length
        ? selectProduct([])
        : selectProduct(displayProducts.map(prod => prod.id))
    );

  const onClickBurgerMenu = () => dispatch(openBurgerMenu(true));

  return (
    <>
      <div className="inventory__header">
        <div className="inventory__functions">
          <div className="left__section">
            <div className="left-section__btns-wrapper">
              <CatalogButton
                name="filter"
                className="filter__button"
                divName="filter__button-div"
              />
              <ChangedFilterButton />
              <CatalogButton
                name="select all"
                divName="select__button-div"
                className="select__button"
                onClick={onSelectAllProducts}
              />
              <ProductQuantity
                totalQuantity={displayProducts.length}
                selectedProductsQuantity={selectedProducts.length}
              />
              {selectedProducts.length > 0 && (
                <CatalogButton
                  name="clear"
                  className="display__none--permanent"
                  onClick={onClearSelectedProducts}
                />
              )}
              <ChangedSelectButton onClick={onSelectOrClear} />
            </div>
          </div>
          <div className="right__section">
            <div className="right-section__btns-wrapper">
              <InventorySearchInput
                inputClassName="inventory__search-input"
                buttonClassName="inventory__search-button"
              />
              <CatalogButton
                name="export products"
                className="inventory__buttons"
              />
              <CatalogButton
                name="add to cart"
                className="inventory__buttons"
              />
              <CatalogButton
                name="remove from my inventory"
                className="inventory__buttons"
                onClick={onClickRemoveButton}
              />
              <ChangedButton className="fas fa-file-export fa-lg export__btn" />
              <ChangedButton className="fas fa-cart-plus fa-lg add__btn" />
              <ChangedButton
                className="far fa-trash-alt fa-lg remove__btn"
                onClick={onClickRemoveButton}
              />
              <BurgerMenu onClick={onClickBurgerMenu}/>
              <HelpButton />
            </div>
          </div>
        </div>
        <div className="inventory__sort">
          <Sort />
        </div>
      </div>
    </>
  );
};
export default InventoryHeader;
