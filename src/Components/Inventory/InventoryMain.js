import "./InventoryMain.css";
import InventoryHeader from "./InventoryHeader";
import InventoryBody from "./InventoryProductsWrapper";
import ModalProduct from "../Common/ModalProduct";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import ProductService from "../Common/ProductService";
import OpenedBurgerMenu from "../Common/OpenedBurgerMenu";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { removeFromCart, getCart } from "../API/API";
import { makeStyles } from "@material-ui/core/styles";
import { useSnackbar } from "notistack";
import { removeSelectedProductsFromCart } from "../API/HelperFunctions";
import { useSelector, useDispatch } from "react-redux";
import { filterProducts } from "../../Redux/FilterFunctions";
import {
  getOriginalProducts,
  getDisplayProducts,
  selectProduct,
  setModalProductId,
  openOrCloseProductServiceModal,
} from "../../Redux/ProductsReducer/ProductsActions";

const useStyles = makeStyles(theme => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
}));

const InventoryMain = () => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();
  
  const { productService, id } = useParams();

  const [openBackDrop, setOpenBackDrop] = useState(false);
  const selectedProducts = useSelector(
    state => state.products.selectedProducts
  );
  const modalProductId = useSelector(state => state.products.modalProductId);
  const isOpenProductService = useSelector(
    state => state.products.isOpenProductService
  );
  const originalList = useSelector(state => state.products.originalProducts);
  const filterOptions = useSelector(state => state.products.filterOptions);
  const dispatch = useDispatch();

  
  useEffect(() => {
    if (Number.isInteger(Number(productService)))
      dispatch(setModalProductId(productService));
    else if ((productService && id) || productService)
      dispatch(openOrCloseProductServiceModal(true));
  }, [productService, id, dispatch]);

  
  useEffect(() => {
    getCart().then(list => dispatch(getOriginalProducts(list)));
  }, [productService, id, dispatch]);

  
  useEffect(() => {
    let array = filterProducts(originalList, filterOptions);
    dispatch(getDisplayProducts(array));
  }, [filterOptions, dispatch, originalList]);

  
  const removeSelectedProducts = () => {
    if (selectedProducts.length > 0) {
      setOpenBackDrop(true);
      removeSelectedProductsFromCart(selectedProducts)
        .then(() => {
          dispatch(selectProduct([]));
          getCart().then(list => {
            dispatch(getOriginalProducts(list));
            dispatch(getDisplayProducts(list));
          });
          enqueueSnackbar("Products removed successfully", {
            variant: "success",
            autoHideDuration: 1500,
          });
        })
        .catch(err => {
          enqueueSnackbar("Products didn't remove successfully", {
            variant: "error",
            autoHideDuration: 1500,
          });
        })
        .finally(() => setOpenBackDrop(false));
    }
  };

  const removeSingleProduct = id => {
    removeFromCart(id)
      .then(data => {
        if (data.success) {
          getCart().then(list => {
            dispatch(getOriginalProducts(list));
            dispatch(getDisplayProducts(list));
          });
          enqueueSnackbar("Product removed successfully", {
            variant: "success",
            autoHideDuration: 2000,
          });
        }
      })
      .catch(err =>
        enqueueSnackbar("Product didn't remove successfully", {
          variant: "error",
          autoHideDuration: 2000,
        })
      );
  };

  return (
    <div className="inventory__main">
      <InventoryHeader onClickRemoveButton={removeSelectedProducts} />
      <InventoryBody removeProduct={removeSingleProduct} />
      <Backdrop className={classes.backdrop} open={openBackDrop}>
        <CircularProgress color="inherit" />
      </Backdrop>
      {modalProductId && (
        <ModalProduct
          productId={modalProductId}
          buttonName="Remove All From Inventory"
          buttonAction={removeSingleProduct}
        />
      )}
      {isOpenProductService && (
        <ProductService service={productService} id={id} />
      )}
      <OpenedBurgerMenu />
    </div>
  );
};
export default InventoryMain;
