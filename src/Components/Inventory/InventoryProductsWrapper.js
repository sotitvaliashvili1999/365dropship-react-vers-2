import "./InventoryProductsWrapper.css";
import Product from "../Common/Product";
import LoadingGif from "../Common/LoadingGif";
import LoadGif from "../../icons/modal Loading.gif";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { useSelector } from "react-redux";
import { useEffect, useState } from "react";

const useStyles = makeStyles({
  container: {
    width: "95%",
    marginTop: "20px",
  },
  item: {
    minHeight: "450px",
    maxHeight: "480px",
  },
});

const InventoryBody = ({ removeProduct }) => {
  const classes = useStyles();
  const [isInventoryEmpty, setIsInventoryEmpty] = useState(false);
  const displayProducts = useSelector(state => state.products.displayProducts);

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (displayProducts.length === 0) setIsInventoryEmpty(true);
    }, 500);
    return () => clearTimeout(timeout);
  }, [displayProducts]);

  return (
    <div className="inventory__body">
      {displayProducts.length === 0 && !isInventoryEmpty && (
        <LoadingGif gif={LoadGif} />
      )}
      {displayProducts.length === 0 && isInventoryEmpty && (
        <div className="empty-inventory"> Inventory is empty</div>
      )}
      {displayProducts.length !== 0 &&
        displayProducts[0].image &&
        !isInventoryEmpty && (
          <Grid container spacing={3} className={classes.container}>
            {displayProducts.map(product => (
              <Grid
                item
                className={classes.item}
                key={product.id}
                xs={12}
                sm={6}
                lg={4}
                xl={3}
                xxl={2}
              >
                <Product
                  id={product.id}
                  key={product.id}
                  img={product.image}
                  title={product.title}
                  price={product.price}
                  qty={Number(product.qty)}
                  buttonName={"Remove Product"}
                  buttonAction={removeProduct}
                />
              </Grid>
            ))}
          </Grid>
        )}
    </div>
  );
};
export default InventoryBody;
