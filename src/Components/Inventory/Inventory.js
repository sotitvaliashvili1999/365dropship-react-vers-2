import "./Inventory.css";
import { useEffect } from "react";
import { tokenValidity, getCart } from "../API/API";
import Aside from "../Aside/Aside";
import InventoryMain from "./InventoryMain";
import { useRouteMatch, Switch, Route } from "react-router";
import { useDispatch } from "react-redux";
import {
  getOriginalProducts,
  resetToInitialState,
} from "../../Redux/ProductsReducer/ProductsActions";

const Inventory = () => {
  const { path } = useRouteMatch();
  const dispatch = useDispatch();

  useEffect(() => {
    tokenValidity();
  }, []);

  useEffect(() => {
    return () => dispatch(resetToInitialState());
  }, [dispatch]);

  useEffect(() => {
    getCart().then(list => dispatch(getOriginalProducts(list)));
  }, [dispatch]);

  
  return (
    <div className="inventory__wrapper">
      <Switch>
        <Route path={`${path}/:productService?/:id?`}>
          <Aside />
          <InventoryMain />
        </Route>
      </Switch>
    </div>
  );
};
export default Inventory;
