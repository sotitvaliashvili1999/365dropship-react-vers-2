import "./InventorySearchInput.css";
import { useState } from "react";
import { getSearchQuery } from "../../Redux/ProductsReducer/ProductsActions";
import { useDispatch } from "react-redux";

const InventorySearchInput = () => {
  const [query, setQuery] = useState("");
  const dispatch = useDispatch();

  const queryChanged = e => setQuery(e.target.value);

  const searchBtnClicked = e => {
    e.preventDefault();
    dispatch(getSearchQuery(query));
  };

  return (
    <div className="inventory-search-input__container">
      <form
        className="inventory-search-input__form"
        onSubmit={searchBtnClicked}
      >
        <input
          className="inventory__search-input"
          type="text"
          id="searchQuery"
          placeholder="Seacrh..."
          value={query}
          onChange={queryChanged}
        />
        <div className="inventory__search-button" onClick={searchBtnClicked}>
          <i className="fas fa-search fa-lg"></i>
        </div>
      </form>
    </div>
  );
};
export default InventorySearchInput;
