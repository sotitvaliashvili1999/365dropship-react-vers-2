import "./CatalogButton.css";
const CatalogButton = ({ name, divName, className, onClick}) => {
  return (
    <div className={"catalog__button-div " + divName} onClick={onClick}>
      <button className={"catalog__button " + className} >{name}</button>
    </div>
  );
};
export default CatalogButton;
