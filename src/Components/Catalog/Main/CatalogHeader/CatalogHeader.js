import "./CatalogHeader.css";
import CatalogButton from "../../CatalogButton";
import ChangedFilterButton from "./ChangedFilterButton";
import ProductQuantity from "./ProductQuantity";
import ChangedSelectButton from "./ChangedSelectButton";
import SearchInput from "./SearchInput";
import BurgerMenu from "../../../Common/BurgerMenu";
import HelpButton from "../../../Common/HelpButton";
import Sort from "./Sort";
import { useSnackbar } from "notistack";
import { addSelectedProductsToCart } from "../../../API/HelperFunctions";
import { useSelector, useDispatch } from "react-redux";
import { selectProduct } from "../../../../Redux/ProductsReducer/ProductsActions";
import { openBurgerMenu} from "../../../../Redux/UIReducer/UIActions";
import { useHistory } from "react-router";
import { useEffect, useState } from "react";

const CatalogHeader = ({ setOpenBackDrop }) => {
  const { enqueueSnackbar } = useSnackbar();
  const [isAdmin, setIsAdmin] = useState(false);
  const displayProducts = useSelector(state => state.products.displayProducts);
  const selectedProducts = useSelector(
    state => state.products.selectedProducts
  );
  const dispatch = useDispatch();
  const { push, location } = useHistory();

  useEffect(() => {
    const admin = localStorage.getItem("isAdmin");
    if (admin === "true") setIsAdmin(true);
    else setIsAdmin(false);
  }, []);

  const onClearSelectedProducts = () => dispatch(selectProduct([]));

  const onSelectAllProducts = () =>
    dispatch(selectProduct(displayProducts.map(prod => prod.id)));

  const onSelectOrClear = () =>
    dispatch(
      selectedProducts.length === displayProducts.length
        ? selectProduct([])
        : selectProduct(displayProducts.map(prod => prod.id))
    );

  const onClickAddToInventory = () => {
    if (selectedProducts.length > 0) {
      setOpenBackDrop(true);
      addSelectedProductsToCart(selectedProducts)
        .then(() => {
          dispatch(selectProduct([]));
          enqueueSnackbar("Products added successfully", {
            variant: "success",
            autoHideDuration: 1500,
          });
        })
        .catch(err => {
          enqueueSnackbar("Products didn't add successfully", {
            variant: "error",
            autoHideDuration: 1500,
          });
        })
        .finally(() => setOpenBackDrop(false));
    }
  };

  const onClickAddNewProduct = () => push(`${location.pathname}/productAdd`);

  const onClickBurgerMenu = () => dispatch(openBurgerMenu(true));

  return (
    <div className="catalog__header">
      <div className="catalog__functions">
        <div className="functions__left-section">
          <div className="left-section__btns-wrapper">
            <CatalogButton
              name="filter"
              className="filter__button"
              divName="filter__button-div"
            />
            <ChangedFilterButton />
            <CatalogButton
              name="select all"
              divName="select__button-div"
              className="select__button"
              onClick={onSelectAllProducts}
            />
            <ProductQuantity
              totalQuantity={displayProducts.length}
              selectedProductsQuantity={selectedProducts.length}
            />
            {selectedProducts.length !== 0 && (
              <CatalogButton
                name="clear"
                className="display__none--permanent"
                onClick={onClearSelectedProducts}
              />
            )}
            <ChangedSelectButton onClick={onSelectOrClear} />
          </div>
        </div>
        <div className="functions__right-section">
          <div className="right-section__input-btns-wrapper">
            <SearchInput />
            <CatalogButton
              name="add to inventory"
              className="add-item__button--big"
              onClick={onClickAddToInventory}
            />
            <CatalogButton
              name="add"
              className="add-item__button--small"
              onClick={onClickAddToInventory}
            />
            <BurgerMenu onClick={onClickBurgerMenu}/>
            <HelpButton />
          </div>
        </div>
      </div>
      <div className="catalog__sort">
        <Sort />
        {isAdmin && (
          <CatalogButton
            name="add new product"
            className="add-new-product__button"
            onClick={onClickAddNewProduct}
          />
        )}
      </div>
    </div>
  );
};
export default CatalogHeader;
