import "./ProductQuantity.css";
const ProductAmount = ({totalQuantity, selectedProductsQuantity}) => {
  return (
    <div className="select__selected-items" id="selected-items__number">
      <span className="selected-items__amount selected-items__before">
        selected {selectedProductsQuantity} out of {totalQuantity} products
      </span>
      <span className="selected-items__amount selected-items__after">
        {totalQuantity} products
      </span>
    </div>
  );
};

export default ProductAmount;
