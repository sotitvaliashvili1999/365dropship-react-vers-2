import "./ChangedFilterButton.css";
const ChangedFilterButton = () => {
  return (
    <div className="filter__button--changed">
        <i className="fas fa-filter filter__icon"></i>
    </div>
  );
};
export default ChangedFilterButton;
