import "./SearchInput.css";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { getSearchQuery } from "../../../../Redux/ProductsReducer/ProductsActions";

const SearchInput = () => {
  const [query, setQuery] = useState("");
  const dispatch = useDispatch();

  const queryChanged = e => setQuery(e.target.value);

  const searchBtnClicked = e => {
    e.preventDefault();
    dispatch(getSearchQuery(query));
  };

  return (
    <div className="search-input-icon__container">
      <form className="search-input__form" onSubmit={searchBtnClicked}>
        <input
          className="search__input"
          type="text"
          id="searchQuery"
          placeholder="Seacrh..."
          value={query}
          onChange={queryChanged}
        />
        <div className="search__button" onClick={searchBtnClicked}>
          <i className="fas fa-search fa-lg"></i>
        </div>
      </form>
    </div>
  );
};
export default SearchInput;
