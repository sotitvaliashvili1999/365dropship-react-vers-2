import "./Sort.css";
import sortIcon from "../../../../icons/sort.png";
import { useDispatch } from "react-redux";
import { getSortType } from "../../../../Redux/ProductsReducer/ProductsActions";

const SortWrapper = () => {
  const dispatch = useDispatch();

  const sortChanged = e => {
    dispatch(getSortType(e.target.value));
  };

  return (
    <div className="sort__wrapper">
      <div className="sort__icon-title-wrapper">
        <div className="sort__icon">
          <img src={sortIcon} alt="sort.png" className="sort__icon--img"></img>
        </div>
        <div className="sort__title">Sort By:</div>
      </div>
      <select id="sorting" className="sort__options" onChange={sortChanged}>
        <option value="">New Arrivals</option>
        <option value="ascPrice">Price: Low To High</option>
        <option value="descPrice">Price: High To Low</option>
        <option value="ascAlphab">Alphabet: Low To High</option>
        <option value="descAlphab">Alphabet: High To Low</option>
      </select>
    </div>
  );
};

export default SortWrapper;
