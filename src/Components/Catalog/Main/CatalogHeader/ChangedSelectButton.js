import icon from "../../../../icons/check1.png";
import "./ChangedSelectButton.css";
import "../../CatalogButton.css";

const ChangedSelectButton = ({onClick}) => {
  return (
    <div className="catalog__button select__button--changed" onClick={onClick} >
        <img src={icon} alt="check.png" className="sel-btn__changed--img"></img>
    </div>
  );
};
export default ChangedSelectButton;
