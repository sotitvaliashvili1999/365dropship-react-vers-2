import "./CatalogBody.css";
import LoadingGif from "../../../Common/LoadingGif";
import LoadGif from "../../../../icons/modal Loading.gif";
import Product from "../../../Common/Product";
import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/styles";
import { useSelector } from "react-redux";
import { Grid } from "@material-ui/core";

const useStyles = makeStyles({
  container: {
    width: "95%",
    marginTop: "20px",
  },
  item: {
    minHeight: "450px",
    maxHeight: "480px",
  },
});

const CatalogBody = ({ addProductToInventory }) => {
  const classes = useStyles();
  const [notFound, setNotFound] = useState(false);
  const displayProducts = useSelector(state => state.products.displayProducts);

  useEffect(() => {
    if (displayProducts.length === 0) {
      const searchProductTime = setTimeout(() => setNotFound(true), 7000);
      setNotFound(false);
      return () => clearTimeout(searchProductTime);
    }
  }, [displayProducts]);

  return (
    <div className="catalog__body">
      {displayProducts.length === 0 && !notFound && (
        <LoadingGif gif={LoadGif} />
      )}
      {displayProducts.length === 0 && notFound && (
        <div className="not-found" id="not-found">
          Products not found
        </div>
      )}
      {displayProducts.length !== 0 && displayProducts[0].imageUrl && (
        <Grid container spacing={3} className={classes.container}>
          {displayProducts.length > 0 &&
            displayProducts.map(product => (
              <Grid
                item
                className={classes.item}
                key={product.id}
                xs={12}
                sm={6}
                lg={4}
                xl={3}
                xxl={2}
              >
                <Product
                  id={product.id}
                  key={product.id}
                  img={product.imageUrl}
                  title={product.title}
                  price={product.price}
                  buttonName={"Add to inventory"}
                  buttonAction={addProductToInventory}
                  qty={1}
                />
              </Grid>
            ))}
        </Grid>
      )}
    </div>
  );
};
export default CatalogBody;
