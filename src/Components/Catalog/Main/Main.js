import "./Main.css";
import CircularProgress from "@material-ui/core/CircularProgress";
import CatalogHeader from "./CatalogHeader/CatalogHeader";
import ModalProduct from "../../Common/ModalProduct";
import ProductService from "../../Common/ProductService";
import CatalogBody from "./CatalogBody/CatalogBody";
import Backdrop from "@material-ui/core/Backdrop";
import OpenedBurgerMenu from "../../Common/OpenedBurgerMenu";
import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { addToCart } from "../../API/API";
import { makeStyles } from "@material-ui/core/styles";
import { useSnackbar } from "notistack";
import { filterProducts } from "../../../Redux/FilterFunctions";
import { getAllProducts } from "../../API/API";
import { useSelector, useDispatch } from "react-redux";
import {
  getOriginalProducts,
  getDisplayProducts,
  setModalProductId,
  openOrCloseProductServiceModal,
} from "../../../Redux/ProductsReducer/ProductsActions";

const useStyles = makeStyles(theme => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
}));

const Main = () => {
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const [openBackDrop, setOpenBackDrop] = useState(false);
  const modalProductId = useSelector(state => state.products.modalProductId);
  const originalList = useSelector(state => state.products.originalProducts);
  const filterOptions = useSelector(state => state.products.filterOptions);
  const dispatch = useDispatch();

  const { productService, id } = useParams();

  useEffect(() => {
    if (Number.isInteger(Number(productService)))
      dispatch(setModalProductId(productService));
    else if ((productService && id) || productService)
      dispatch(openOrCloseProductServiceModal(true));
  }, [productService, id, dispatch]);

  useEffect(() => {
    getAllProducts()
      .then(list => dispatch(getOriginalProducts(list)))
      .catch(err =>
        enqueueSnackbar("Products hasn't been fetched successfully", {
          variant: "error",
          autoHideDuration: 2000,
        })
      );
  }, [productService, id, dispatch, enqueueSnackbar]);

  useEffect(() => {
    let array = filterProducts(originalList, filterOptions);
    dispatch(getDisplayProducts(array));
  }, [filterOptions, dispatch, originalList]);

  const addProductToInventory = (id, qty) => {
    addToCart(id, qty)
      .then(() =>
        enqueueSnackbar("Product has been added successfully", {
          variant: "success",
          autoHideDuration: 1500,
        })
      )
      .catch(err =>
        enqueueSnackbar("Product hasn't been added successfully", {
          variant: "error",
          autoHideDuration: 1500,
        })
      );
  };

  return (
    <main className="main">
      <CatalogHeader setOpenBackDrop={setOpenBackDrop} />
      <CatalogBody addProductToInventory={addProductToInventory} />
      <Backdrop className={classes.backdrop} open={openBackDrop}>
        <CircularProgress color="inherit" />
      </Backdrop>
      {modalProductId && (
        <ModalProduct
          productId={modalProductId}
          qty={1}
          buttonName="add to my inventory"
          buttonAction={addProductToInventory}
        />
      )}
      <ProductService service={productService} id={id} />
      <OpenedBurgerMenu />
    </main>
  );
};
export default Main;
