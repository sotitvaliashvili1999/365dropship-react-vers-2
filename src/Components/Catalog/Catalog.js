import "./Catalog.css";
import Aside from "../Aside/Aside";
import Main from "./Main/Main";
import { getAllProducts, tokenValidity } from "../API/API";
import { useEffect } from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { useDispatch } from "react-redux";
import { useSnackbar } from "notistack";
import {
  getOriginalProducts,
  resetToInitialState,
} from "../../Redux/ProductsReducer/ProductsActions";

const Catalog = () => {
  const { path } = useRouteMatch();
  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();

  useEffect(() => {
    tokenValidity();
  }, []);

  useEffect(() => {
    return () => dispatch(resetToInitialState());
  }, [dispatch]);

  useEffect(() => {
    getAllProducts()
      .then(list => dispatch(getOriginalProducts(list)))
      .catch(err =>
        enqueueSnackbar("Products hasn't been fetched successfully", {
          variant: "error",
          autoHideDuration: 2000,
        })
      );
  }, [dispatch, enqueueSnackbar]);

  return (
    <div className="catalog">
      <Switch>
        <Route path={`${path}/:productService?/:id?`}>
          <Aside />
          <Main />
        </Route>
      </Switch>
    </div>
  );
};
export default Catalog;
