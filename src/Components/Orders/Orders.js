import Tint from "../Common/Tint";
import {  useEffect } from "react";
import { tokenValidity } from "../API/API";
const Orders = () =>{
    useEffect(() => {
        tokenValidity();
      }, []);
    return (
        <div className="temporary__wrapper">
      <Tint />
      <h1 className="temporary__header">O R D E R</h1>
    </div>
    );
};
export default Orders;