import "./ProfileIcon.css";
import profileIcon from "../../icons/profile.png";

const ProfileIcon = () => {
  return (
    <div className="nav-item__container">
      <img
        src={profileIcon}
        alt="ProfileIcon.png"
        className="nav__item-img nav__item--profile"
      />
    </div>
  );
};
export default ProfileIcon;
