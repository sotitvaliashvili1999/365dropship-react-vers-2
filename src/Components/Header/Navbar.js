import { NavLink } from "react-router-dom";
import "./Navbar.css";
import NavItem from "./NavItem";
import ProfileIcon from "./ProfileIcon";
const Navbar = () => {
  return (
    <nav className="navbar">
      <NavLink to="/profile" activeClassName="nav__item--active">
        <ProfileIcon />
      </NavLink>
      <NavLink to="/dashboard" activeClassName="nav__item--active">
        <NavItem name="dashboard" />
      </NavLink>
      <NavLink to="/catalog" activeClassName="nav__item--active">
        <NavItem name="catalog" />
      </NavLink>
      <NavLink to="/inventory" activeClassName="nav__item--active">
        <NavItem name="inventory" />
      </NavLink>
      <NavLink to="/cart" activeClassName="nav__item--active">
        <NavItem name="cart" />
      </NavLink>
      <NavLink to="/orders" activeClassName="nav__item--active">
        <NavItem name="orders" />
      </NavLink>
      <NavLink to="/transactions" activeClassName="nav__item--active">
        <NavItem name="transactions" />
      </NavLink>
      <NavLink to="/shoplist" activeClassName="nav__item--active">
        <NavItem name="shoplist" />
      </NavLink>
    </nav>
  );
};

export default Navbar;
