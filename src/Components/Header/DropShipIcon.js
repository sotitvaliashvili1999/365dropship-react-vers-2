import "./DropShipIcon.css";
import Icon from "../../icons/365Icon.png";
import { Link } from "react-router-dom";
const DropShipIcon = () => {
  return (
    <div className="web-page__icon-div">
      <Link to="/homepage">
        <img
          src={Icon}
          alt="365DropShipICON.png"
          className="web-page__icon"
        ></img>
      </Link>
    </div>
  );
};
export default DropShipIcon;
