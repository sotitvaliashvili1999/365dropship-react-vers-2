import "./NavItem.css";
import { useState } from "react";
const NavItem = ({ name }) => {
  const [navItemList] = useState({
    dashboard: "fa-lg fas fa-tachometer-alt",
    catalog: "fa-lg fas fa-list-ul",
    inventory: "fa-lg fas fa-box",
    cart: "fa-lg fas fa-shopping-cart",
    orders: "fa-lg fas fa-archive",
    transactions: "fa-lg fas fa-exchange-alt",
    shoplist: "fa-lg fas fa-clipboard-list",
  });
  return (
    <div className="nav-item__container active">
      <i className={"nav-item__icon " + navItemList[name]}></i>
    </div>
  );
};
export default NavItem;
