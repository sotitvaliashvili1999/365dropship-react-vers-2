import "./Header.css";
import Navbar from "./Navbar";
import DropShipIcon from "./DropShipIcon";
const Header = () => {
  return (
    <header className="header">
      <DropShipIcon />
      <Navbar />
    </header>
  );
};
export default Header;
